from bs4 import BeautifulSoup
import urllib.request as urllib
from random import *
import json

url_intro = urllib.urlopen("http://www.procjam.com/seeds/issues/3/")
url_articles = [
    "http://www.procjam.com/seeds/issues/3/",
    "http://www.procjam.com/seeds/issues/3/ai.txt.html",
    "http://www.procjam.com/seeds/issues/3/amin.txt.html",
    "http://www.procjam.com/seeds/issues/3/ant.txt.html",
    "http://www.procjam.com/seeds/issues/3/apop.txt.html",
    "http://www.procjam.com/seeds/issues/3/avoyd.txt.html",
    "http://www.procjam.com/seeds/issues/3/bugs.txt.html",
    "http://www.procjam.com/seeds/issues/3/cox.txt.html",
    "http://www.procjam.com/seeds/issues/3/data.txt.html",
    "http://www.procjam.com/seeds/issues/3/davide.txt.html",
    "http://www.procjam.com/seeds/issues/3/gorm.txt.html",
    "http://www.procjam.com/seeds/issues/3/guppy.txt.html",
    "http://www.procjam.com/seeds/issues/3/hache.txt.html",
    "http://www.procjam.com/seeds/issues/3/hacker.txt.html",
    "http://www.procjam.com/seeds/issues/3/isaac.txt.html",
    "http://www.procjam.com/seeds/issues/3/isl.txt.html",
    "http://www.procjam.com/seeds/issues/3/jewel.txt.html",
    "http://www.procjam.com/seeds/issues/3/koz.txt.html",
    "http://www.procjam.com/seeds/issues/3/kth.txt.html",
    "http://www.procjam.com/seeds/issues/3/lac.txt.html",
    "http://www.procjam.com/seeds/issues/3/mark.txt.html",
    "http://www.procjam.com/seeds/issues/3/mewo.txt.html",
    "http://www.procjam.com/seeds/issues/3/minus.txt.html",
    "http://www.procjam.com/seeds/issues/3/moo.txt.html",
    "http://www.procjam.com/seeds/issues/3/mount.txt.html",
    "http://www.procjam.com/seeds/issues/3/name.txt.html",
    "http://www.procjam.com/seeds/issues/3/news.txt.html",
    "http://www.procjam.com/seeds/issues/3/nos.txt.html",
    "http://www.procjam.com/seeds/issues/3/not.txt.html",
    "http://www.procjam.com/seeds/issues/3/npc.txt.html",
    "http://www.procjam.com/seeds/issues/3/odd.txt.html",
    "http://www.procjam.com/seeds/issues/3/pech.txt.html",
    "http://www.procjam.com/seeds/issues/3/pgod.txt.html",
    "http://www.procjam.com/seeds/issues/3/rand.txt.html",
    "http://www.procjam.com/seeds/issues/3/red.txt.html",
    "http://www.procjam.com/seeds/issues/3/rende.txt.html",
    "http://www.procjam.com/seeds/issues/3/rot.txt.html",
    "http://www.procjam.com/seeds/issues/3/samim.txt.html",
    "http://www.procjam.com/seeds/issues/3/spell.txt.html",
    "http://www.procjam.com/seeds/issues/3/strata.txt.html",
    "http://www.procjam.com/seeds/issues/3/til.txt.html",
    "http://www.procjam.com/seeds/issues/3/tiny.txt.html",
    "http://www.procjam.com/seeds/issues/3/writing.txt.html"
]

#content = url_intro.read()
#soup = BeautifulSoup(content)

seeds_content = []
article_count = 0
#mon_texte = soup.find_all(["h2", "p", 'ul'])

for a in url_articles:
    url = urllib.urlopen(a)
    content = url.read()
    soup = BeautifulSoup(content)
    title = []
    article_content = []
    summary = []
    for tag in soup.find_all(True):
        if tag.name == 'h2':
            h2_cont = tag.get_text()
            h2 = {'type': 'h2', 'content': h2_cont}
            title.append(h2)
        elif tag.name == 'p':
            if len(tag.get_text()) < 1:
                pass
            else:
                p_cont = tag.get_text()
                p = {'type': 'p', 'content': p_cont}
                article_content.append(p)
                for t in tag.children:
                    if tag.name == 'a':
                        a_cont = tag.get_text()
                        link = tag.get('href')
                        a = {'type': 'a', 'content': a_cont, 'link': link}
                        article_content.append(a)
        elif tag.name == 'img':
            src = tag.get('src')
            url_full = "http://www.procjam.com/seeds/issues/3/%s" %src
            img = {'type': 'img', 'url': url_full}
            article_content.append(img)
        elif tag.name == 'iframe':
            src = tag.get('src')
            iframe = {'type': 'iframe', 'url': src}
            article_content.append(iframe)
        elif tag.name == 'li':
            if article_count == 0:
                li_cont = tag.get_text()
                li = {'type': 'li', 'content': li_cont}
                summary.append(li)
            else:
                li_cont = tag.get_text()
                li = {'type': 'li', 'content': li_cont}
                article_content.append(li)
        elif tag.name == 'h3':
            if article_count == 0:
                pass
            else:
                h3_cont = tag.get_text()
                h3 = {'type': 'h3', 'content': h3_cont}
                article_content.append(h3)
        elif tag.name == 'i':
            if article_count == 0:
                pass
            else:
                i_cont = tag.get_text()
                i = {'type': 'i', 'content': i_cont}
                article_content.append(i)
        elif tag.name == 'th':
            th_cont = tag.get_text()
            th = {'type': 'th', 'content': th_cont}
            article_content.append(th)
        elif tag.name == 'td':
            td_cont = tag.get_text()
            td = {'type': 'td', 'content': td_cont}
            article_content.append(tag.get_text())
    if article_count == 0:
        type = "intro"
        seeds_content.append({'type': type, 'index': article_count, 'titre': title, 'contenu': article_content, 'sommaire': summary})
    else:
        type = "article"
        seeds_content.append({'type': type, 'index': article_count, 'titre': title, 'contenu': article_content})
    article_count += 1

print(seeds_content)

with open('data.json', 'w') as f:
    json.dump(seeds_content, f, indent=4)
