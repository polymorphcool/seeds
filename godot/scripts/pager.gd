extends Control

onready var seeds:Node = get_node( "/root/seeds" )

enum ELEMENT_TYPE { ET_UNDEFINED, ET_IMAGE, ET_TEXT }
enum SYNC_POSITION { SP_UNDEFINED, SP_CONTROL2PHYSICS, SP_PHYSICS2CONTROL }

export (NodePath) var json_loader:NodePath
export (NodePath) var menu:NodePath
export (float,0,30) var pull_time:float = 4
export (float,0,30) var history_depth:float = 5
export (float,0,100) var border_thickness:float = 30
export (float,0,2) var border_multiply:float = 1.6
export (float,0,200) var highlight_margin:float = 20

onready var jloader:Node = 	get_node(json_loader)
onready var mn:Node = 		get_node(menu)

onready var circle:Line2D = $tmpl/circle
onready var body:RigidBody2D = $tmpl/rigid
onready var kine:RigidBody2D = $tmpl/kinematic
onready var backup:RigidBody2D = $tmpl/backup
var elements:Array = []
var bb_min:Vector2 = Vector2.ZERO
var bb_max:Vector2 = Vector2.ZERO
var bb_size:Vector2 = Vector2.ZERO
var bb_center:Vector2 = Vector2.ZERO
var editor_physics:bool = false

var curr_pull_time:float = 0
var sync_elements:bool = false
onready var vp_size:Vector2 = get_viewport().size
onready var page_center:Vector2 = get_viewport().size * 0.5
onready var backup_color:Color = seeds.COLOR_GENERATOR.get_random()

func purge_hover():
	while $hover.get_child_count() > 0:
		var c:Node = $hover.get_child(0)
		$hover.remove_child(c)
		c.queue_free()

func purge_hightlight():
	while $hightlight.get_child_count() > 0:
		var c:Node = $hightlight.get_child(0)
		seeds.focus_unlock( c )
		$hightlight.remove_child(c)
		c.queue_free()

func new_backup_element( position:Vector2, size:Vector2, mult:float, vshift:float ):
	var bu:RigidBody2D = backup.duplicate()
	var coll:CollisionShape2D = bu.get_child(0)
	coll.shape = coll.shape.duplicate()
	var border:Panel = bu.get_child(1)
	var stylb:StyleBoxFlat = border.get("custom_styles/panel").duplicate()
	border.set("custom_styles/panel",stylb)
	var disp:Panel = bu.get_child(2)
	var styld:StyleBoxFlat = disp.get("custom_styles/panel").duplicate()
	disp.set("custom_styles/panel",styld)
	stylb.bg_color = seeds.COLOR_GENERATOR.shift(backup_color,rand_range(-0.1,0.1),0,0.1)
	styld.bg_color = seeds.COLOR_GENERATOR.shift(backup_color,rand_range(-0.1,0.1),0,vshift)
	var bwidth:float = rand_range(3,5)
	bu.position = position
	coll.shape.extents = size * mult * .5
	disp.rect_size = size * mult
	disp.rect_position = size * -mult * .5
	border.rect_size = disp.rect_size + Vector2.ONE * bwidth * 2
	border.rect_rotation = rand_range(-1,1)
	border.rect_position = disp.rect_position - Vector2( rand_range(0,bwidth), rand_range(0,bwidth) )
	return bu

func backup_elements():
	var currh:Node = null
	for i in range(0,$physics/history.get_child_count()):
		currh = $physics/history.get_child(i)
		# cleanup of all children
		while currh.get_child_count() > 0:
			var c:Node = currh.get_child( 0 )
			currh.remove_child( c )
			c.queue_free()
		if i < history_depth-1:
			# copying all elements form previous version into this one
			var nexth:Node = $physics/history.get_child(i+1)
			var vshift:float = 0
			for c in nexth.get_children():
				# getting colors of border and display
				var size:Vector2 = c.get_child(2).rect_size
				currh.add_child( new_backup_element( c.position, size, 0.6, vshift ) )
				vshift -= rand_range( 0.01, 0.03 )
	
	backup_color = seeds.COLOR_GENERATOR.shift(backup_color,0.2,0,0)
	var vshift:float = 0
	
	# backup current elements
	for e in elements:
		currh.add_child( new_backup_element( e.node.rect_position, e.size, 0.25, vshift ) )
		vshift -= rand_range( 0.01, 0.03 )

func purge():
	backup_elements()
	while $physics/current.get_child_count() > 0:
		$physics/current.remove_child($physics/current.get_child(0))
	while $elements.get_child_count() > 0:
		var c:Node = $elements.get_child(0)
		$elements.remove_child(c)
		c.queue_free()
	while $circles.get_child_count() > 0:
		$circles.remove_child($circles.get_child(0))
	purge_hightlight()

func delete_body( e:Dictionary ):
	if e.body == null:
		return
	$physics/current.remove_child( e.body )
	e.body.queue_free()
	e.body = null

func create_body( e:Dictionary, tmpl:RigidBody2D, syncp:int = SYNC_POSITION.SP_UNDEFINED ):
	var new_body:bool = false
	if e.body != null and e.body.mode != tmpl.mode:
		delete_body(e)
		new_body = true
	var coll:CollisionShape2D
	if e.body == null:
		e.body = tmpl.duplicate( 0 )
		e.body.visible = true
		coll = e.body.get_child(0)
		coll.shape = coll.shape.duplicate()
		new_body = true
	else:
		coll = e.body.get_child(0)
	if e.body.mode == RigidBody2D.MODE_RIGID:
		coll.shape.radius = e.min_radius
	elif e.body.mode == RigidBody2D.MODE_KINEMATIC:
		coll.shape.radius = e.min_radius
	match syncp:
		SYNC_POSITION.SP_CONTROL2PHYSICS:
			e.body.position = e.node.rect_position + e.offset
		SYNC_POSITION.SP_PHYSICS2CONTROL:
			e.node.rect_position = e.body.position - e.offset
	if new_body:
		$physics/current.add_child( e.body )
	return coll

func create_element( n:Node ):
	var d:Dictionary = {
		'type': 		ELEMENT_TYPE.ET_UNDEFINED,
		'node': 		n,
		'body': 		null,
		'circle': 		null,
		'size':			Vector2.ZERO,
		'offset':		Vector2.ZERO, # half size
		'area': 		0.0,
		'min_radius': 	0.0,
		'max_radius': 	0.0,
		'highlight': 	null,
		'hover': 		null,
		'dragging': 	false
	}
	var sc = n.get_script()
	if sc != null and sc.resource_path.find( "pager_image.gd" ) > -1:
		d.type = ELEMENT_TYPE.ET_IMAGE
	elif sc != null and sc.resource_path.find( "pager_textblock.gd" ) > -1:
		d.type = ELEMENT_TYPE.ET_TEXT
	return d

func create_hover( el:Dictionary ):
	purge_hover()
	match el.type:
		ELEMENT_TYPE.ET_IMAGE:
			var he:TextureRect = TextureRect.new()
			he.mouse_filter = Control.MOUSE_FILTER_IGNORE
			$hover.add_child( he )
			he.expand = 		el.node.expand
			he.rect_size = 		el.node.rect_size
			he.rect_position = 	el.node.rect_position
			he.texture = 		el.node.texture
			el.hover = he
		ELEMENT_TYPE.ET_TEXT:
			var cp:Label = el.node.get_copy()
			$hover.add_child( cp )
			el.hover = cp

func create_highlight( el:Dictionary ):
	purge_hover()
	purge_hightlight()
	if el.hover != null:
		el.hover = null
	match el.type:
		ELEMENT_TYPE.ET_IMAGE:
			var h:TextureRect = seeds.HIGHTLIGHT_IMAGE.instance()
			h.max_size = vp_size - Vector2.ONE * highlight_margin * 2
			h.texture = el.node.texture
			h.pager = self
			$hightlight.add_child( h )
		ELEMENT_TYPE.ET_TEXT:
			var t:PanelContainer = seeds.HIGHTLIGHT_TEXT.instance()
			t.pager = self
			$hightlight.add_child( t )
			t.purge()
			t.reset_style()
			for d in el.node.data:
				if d.type == 'h3':
					 t.add_subtitle( d.content )
				elif d.type == 'p':
					 t.add_paragraph( d.content )
			t.rect_position.x = el.node.rect_position.x + el.offset.x - t.maxw * .5
			if t.rect_position.x < -page_center.x+20:
				t.rect_position.x = -page_center.x+20
			elif t.rect_position.x + t.maxw > page_center.x-20:
				t.rect_position.x = page_center.x-(20+t.maxw)
			t.rect_position.y = -page_center.y

func prepare_element( e ):
	var new_el:bool = false
	e.size = e.node.rect_size
	e.offset = e.size * 0.5
	e.area = e.size.x * e.size.y
	if e.size.x < e.size.y:
		e.min_radius = e.offset.x
		e.max_radius = e.offset.y
	else:
		e.min_radius = e.offset.y
		e.max_radius = e.offset.x
	# circle above element
	if e.circle == null:
		new_el = true
		e.circle = circle.duplicate()
		e.circle.visible = true
		$circles.add_child( e.circle )
	e.circle.set_radius( e.min_radius )
	# creating physics
#		DUPLICATE_SIGNALS = 1
#		DUPLICATE_GROUPS = 2
#		DUPLICATE_SCRIPTS = 4
#		DUPLICATE_USE_INSTANCING = 8
	var coll:CollisionShape2D = null
	if e.body == null:
		new_el = true
		if e.dragging:
			coll = create_body( e, kine )
		else:
			coll = create_body( e, body )
	else:
		coll = e.body.get_child(0)
	if e.body.mode == RigidBody2D.MODE_RIGID:
		coll.shape.radius = e.min_radius
	elif e.body.mode == RigidBody2D.MODE_KINEMATIC:
		coll.shape.radius = e.max_radius
	if new_el:
		# randomising start position
		randomize()
		e.body.position = Vector2( rand_range(-10,10), rand_range(-10,10) )
		e.node.rect_position = e.body.position-e.offset

func prepare():
	# purging physics & circles
	elements = []
	for e in $elements.get_children():
		if ! e is Control:
			continue
		elements.append( create_element(e) ) 
		var lfound:bool = true
		while lfound:
			for i in range(0,e.get_child_count()):
				var c = e.get_child(i)
				if c is Line2D:
					e.remove_child(c)
					lfound = false
					break
			lfound = false
	randomize()
	for e in elements:
		prepare_element( e )
	sort_elements()

func sort_elements():
	# sorting elements
	for e in elements:
		seeds.focus_unregister( e.node )
		$elements.remove_child( e.node )
		self.add_child( e.node )
		e.node.owner = self
	elements.sort_custom(self, "area_compare")
	for e in elements:
		self.remove_child( e.node )
		$elements.add_child( e.node )
		e.node.owner = $elements
		# due to reparenting, element has left tree -> let's register it again as a focus listener
		seeds.focus_register( e.node )

func area_compare(a, b):
	return a.area > b.area
	
func border_resize():
	if $border.get_child_count() != 4:
		var tmpl:Node = $border.get_child(0)
		for i in range (0,3):
			var n:Node = tmpl.duplicate()
			n.get_child(0).shape = tmpl.get_child(0).shape.duplicate()
			match i:
				0:
					n.get_child(1).color = Color(1,0,0)
				1:
					n.get_child(1).color = Color(0,1,0)
				2:
					n.get_child(1).color = Color(0,0,1)
			$border.add_child( n )
	var border_top = 			$border.get_child(0)
	var border_bottom = 		$border.get_child(1)
	var border_left = 			$border.get_child(2)
	var border_right = 			$border.get_child(3)
	border_top.position = 		page_center * Vector2(0,-1) * border_multiply
	border_bottom.position = 	page_center * Vector2(0,1) * border_multiply
	border_left.position = 		page_center * Vector2(-1,0) * border_multiply
	border_right.position = 	page_center * Vector2(1,0) * border_multiply
	border_top.get_child(0).shape.extents = 	Vector2( vp_size.x * border_multiply * 2, border_thickness )
	border_bottom.get_child(0).shape.extents = 	Vector2( vp_size.x * border_multiply * 2, border_thickness )
	border_left.get_child(0).shape.extents = 	Vector2( border_thickness, vp_size.y * border_multiply * 2 )
	border_right.get_child(0).shape.extents = 	Vector2( border_thickness, vp_size.y * border_multiply * 2 )
	for b in $border.get_children():
		b.get_child(1).scale = b.get_child(0).shape.extents

func _ready():
	vp_changed()
# warning-ignore:return_value_discarded
	get_viewport().connect("size_changed",self,"vp_changed")
	# generate history tree
# warning-ignore:unused_variable
	for i in range(0,history_depth):
		var n:Node2D = Node2D.new()
		$physics/history.add_child( n )

func new_block():
	var lbl:Control = seeds.PAGER_TEXT.instance()
	lbl.text = ''
	lbl.set_pager( self )
	$elements.add_child( lbl )
	return lbl

func new_image():
	var im:TextureRect = seeds.PAGER_IMAGE.instance()
	im.set_pager( self )
	$elements.add_child( im )
	return im

func load_page( i:int ):
	print( "loading page ", i )
	var data = jloader.get_page( i )
	if data == null:
		return
	# cleanup page
	purge()
# warning-ignore:unused_variable
	var curr_lbl:Label = null
	var blockid:int = 1
	for el in data.contenu:
		if !'type' in el:
			continue
		if el.type == 'img':
			var im = new_image()
			im.get( el.url )
			curr_lbl = null
		elif el.type == 'h3':
			curr_lbl = new_block()
			curr_lbl.text += '[' + str(blockid) + ']. ' + el.content + '\n'
			curr_lbl.data.append( el )
			blockid += 1
		elif el.type == 'p':
			var xtr:String = ''
			if curr_lbl == null:
				curr_lbl = new_block()
				xtr = '[' + str(blockid) + ']. '
				blockid += 1
			curr_lbl.text += xtr + el.content + '\n'
			curr_lbl.data.append( el )
	prepare()
	reset_timer()

func vp_changed():
	vp_size = get_viewport().size
	page_center = vp_size * 0.5
	rect_position = page_center
	border_resize()

func reset_element( e, force_enable:bool = true ):
	if e.body == null or e.body.mode == RigidBody2D.MODE_KINEMATIC:
		create_body( e, body )
		e.body.position = e.node.rect_position + e.offset
		e.body.linear_velocity = Vector2.ZERO
	e.highlight = null
	e.dragging = false
	e.body.visible = true
	e.circle.visible = true
	if force_enable:
		e.node.enable( true )

func element_resize(n:Node):
	for e in elements:
		if e.node == n:
			prepare_element( e )
			reset_timer()
			return

func element_hover( n:Node, hover:bool ):
	for e in elements:
		if e.node == n:
			if e.highlight != null:
				# element is already highlighted, NO need to hover it!
				continue
			if hover:
				create_hover( e )
			else:
				purge_hover()
				e.hover = null
		elif e.hover != null:
			e.hover = null

func element_pressed( n:Node ):
	for e in elements:
		if e.node == n:
			create_highlight( e )
			delete_body( e )
			e.node.mouse_in = false
			e.node.pressed = false
		else:
			if e.highlight != null:
				e.highlight = null
			if e.body == null:
				create_body( e, body, SYNC_POSITION.SP_CONTROL2PHYSICS )

func element_dragged( n:Node, dragging:bool ):
	for e in elements:
		if e.node == n:
			if e.dragging != dragging:
				e.dragging = dragging
				if dragging:
					create_body( e, kine, SYNC_POSITION.SP_CONTROL2PHYSICS )
				else:
					create_body( e, body, SYNC_POSITION.SP_CONTROL2PHYSICS )
		else:
			if e.dragging:
				e.dragging = false
				create_body( e, body, SYNC_POSITION.SP_CONTROL2PHYSICS )

func hightlight_pressed( n:Node ):
	purge_hightlight()
	for e in elements:
		if e.highlight != null:
			e.highlight = null
		if e.body == null:
			create_body( e, body, SYNC_POSITION.SP_CONTROL2PHYSICS )

func reset_timer():
	curr_pull_time = pull_time
	sync_elements = true

func _process(delta):
	var sort_req:bool = false
	for b in $physics/history.get_children():
		for c in b.get_children():
			c.applied_force = c.position * -delta
#	if sync_elements:
	var ecount:int = elements.size()
	var div:float = pow( elements.size(), .5 )
	var elcount:int = elements.size()
	for e in elements:
		if e.hover != null:
			e.hover.rect_position = e.node.rect_position
		if e.body == null:
			continue
		if e.node.rect_size != e.size:
			prepare_element(e)
			sort_req = true
		if e.dragging:
			e.body.position = e.node.rect_position+e.offset
			e.circle.position = e.node.rect_position+e.offset
			reset_timer()
		else:
			e.circle.position = e.body.position
			e.node.rect_position = e.body.position-e.offset
			# composition bounding box
			e.body.applied_force = Vector2.ZERO
			if elcount > 1:
				e.body.applied_force += (e.body.position * ecount * -delta)
				for o in elements:
					if o == e or o.body == null:
						continue
					var m:float = 1
					if o.dragging:
						m = 3
					e.body.applied_force += (o.body.position - e.body.position) * m / div
	if sort_req:
		sort_elements()
