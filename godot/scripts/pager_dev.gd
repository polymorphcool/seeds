tool

extends Node2D

export (bool) var generate:bool = false setget do_generate
export (bool) var toggle_physics:bool = false setget do_toggle_physics

export (float,0,30) var pull_time:float = 4

var initialised:bool = false
var circle:Line2D = null
var body:RigidBody2D = null
var elements:Array = []
var bb_min:Vector2 = Vector2.ZERO
var bb_max:Vector2 = Vector2.ZERO
var bb_size:Vector2 = Vector2.ZERO
var bb_center:Vector2 = Vector2.ZERO
var editor_physics:bool = false

var curr_pull_time:float = 0
var sync_elements:bool = true
var page_center:Vector2 = Vector2.ZERO
var vp_size:Vector2 = Vector2.ZERO

func do_generate(b:bool):
	generate = false
	if b:
		initialised = false

func do_toggle_physics(b:bool):
	toggle_physics = false
	if b:
		editor_physics = !editor_physics
		Physics2DServer.set_active(editor_physics)

func prepare():
	circle = $tmpl/circle
	body = $tmpl/body
	# purging physics & circles
	while $physics.get_child_count() > 0:
		$physics.remove_child($physics.get_child(0))
	while $circles.get_child_count() > 0:
		$circles.remove_child($circles.get_child(0))
	elements = []
	for e in $elements.get_children():
		if ! e is Control:
			continue
		elements.append( {
			'node': 		e,
			'body': 		null,
			'circle': 		null,
			'size':			Vector2.ZERO,
			'offset':		Vector2.ZERO, # half size
			'area': 		0.0,
			'min_radius': 	0.0,
			'max_radius': 	0.0
		} )
		var lfound:bool = true
		while lfound:
			for i in range(0,e.get_child_count()):
				var c = e.get_child(i)
				if c is Line2D:
					e.remove_child(c)
					lfound = false
					break
			lfound = false
	randomize()
	for e in elements:
		e.size = e.node.rect_size
		e.offset = e.size * 0.5
		e.area = e.size.x * e.size.y
		if e.size.x < e.size.y:
			e.min_radius = e.offset.x
			e.max_radius = e.offset.y
		else:
			e.min_radius = e.offset.y
			e.max_radius = e.offset.x
		# circle above element
		e.circle = circle.duplicate()
		$circles.add_child( e.circle )
		e.circle.set_radius( e.min_radius )
		# creating physics
#		DUPLICATE_SIGNALS = 1
#		DUPLICATE_GROUPS = 2
#		DUPLICATE_SCRIPTS = 4
#		DUPLICATE_USE_INSTANCING = 8
		e.body = body.duplicate( 0 )
		var coll:CollisionShape2D = e.body.get_child(0)
		coll.shape = coll.shape.duplicate()
		coll.shape.radius = e.min_radius
		$physics.add_child( e.body )
		e.body.owner = $physics
		# randomising start position
		randomize()
		e.body.position = Vector2( rand_range(-10,10), rand_range(-10,10) )
		e.node.rect_position = e.body.position-e.offset
	
	# sorting elements
	for e in elements:
		$elements.remove_child( e.node )
		self.add_child( e.node )
		e.node.owner = self
	elements.sort_custom(self, "area_compare")
	for e in elements:
		self.remove_child( e.node )
		$elements.add_child( e.node )
		e.node.owner = $elements

func area_compare(a, b):
	return a.area > b.area

func _ready():
	pass

func vp_changed():
	vp_size = get_viewport().size
	page_center = vp_size * 0.5

func reset_timer():
	curr_pull_time = pull_time
	sync_elements = true
	for e in elements:
		e.body.position = e.node.rect_position+e.offset

func _process(delta):
	
	if !initialised:
		vp_changed()
		self.position = vp_size * 0.5
# warning-ignore:return_value_discarded
		get_viewport().connect("size_changed",self,"vp_changed")
		prepare()
		reset_timer()
		initialised = true
	
	if sync_elements:
		bb_min = Vector2.ZERO
		bb_max = Vector2.ZERO
		bb_center = Vector2.ZERO
		for e in elements:
			e.circle.position = e.body.position
			e.node.rect_position = e.body.position-e.offset
			# composition bounding box
			var emin = e.body.position-e.offset
			var emax = e.body.position+e.offset
			if bb_min.x > emin.x:
				bb_min.x = emin.x
			if bb_min.y > emin.y:
				bb_min.y = emin.y
			if bb_max.x < emax.x:
				bb_max.x = emax.x
			if bb_max.y < emax.y:
				bb_max.y = emax.y
			if curr_pull_time > 0:
				e.body.applied_force = Vector2.ZERO
				e.body.applied_force += e.body.position * -delta
				for o in elements:
					if o == e:
						continue
					e.body.applied_force += o.body.position - e.body.position
		bb_size = bb_max-bb_min
		bb_center = bb_min + bb_size * 0.5
	
	# center composition on screen
	self.position += ( page_center - self.position ) * min(1,delta*5)
		
	if curr_pull_time > 0:
		curr_pull_time -= delta
		if curr_pull_time <= 0:
			for e in elements:
				e.body.applied_force = Vector2.ZERO
			sync_elements = false
	
