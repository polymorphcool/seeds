extends TextureRect

onready var seeds = get_node("/root/seeds")

export (float,0,10) var rspeed:float = 3
export (Vector2) var max_size:Vector2 = Vector2(1000,1000)

var r:float = 0
var pager:Node = null
var mouse_in:bool = false
var pressed:bool = false

func resize_image():
	rect_size = texture.get_size()
	if rect_size.x > max_size.x:
		rect_size.y /= rect_size.x / max_size.x
		rect_size.x = max_size.x
	if rect_size.y > max_size.y:
		rect_size.x /= rect_size.y / max_size.y
		rect_size.y = max_size.y
	rect_position = rect_size * -.5

func _ready():
	resize_image()
	material.set_shader_param("static_color", seeds.COLOR_GENERATOR.get_random() )
	connect("mouse_entered",self,"_on_mouse_entered")
	connect("mouse_exited",self,"_on_mouse_exited")

func _on_mouse_entered():
	mouse_in = true

func _on_mouse_exited():
	mouse_in = false

func _input(event):
	if !mouse_in:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			pressed = true
		elif pressed:
			if pager != null:
				pager.hightlight_pressed(self)
			pressed = false

func _process(delta):
	if r > 2:
		material = null
		return
	material.set_shader_param("limit", r )
	r += rspeed * delta
	pass
