extends Node2D

onready var seeds:Node = get_node("/root/seeds")

export (NodePath) var font_generator:NodePath = ''
export (NodePath) var color_generator:NodePath = ''
export (int,0,15) var pow2:int = 8 
export (bool) var apply_size:bool = false setget do_apply_size
export (bool) var generate:bool = false setget do_generate

onready var size:int = pow(2,pow2)
var requestors:Array = []

func do_apply_size(b:bool):
	apply_size = false
	if b:
		print( pow(2,pow2) )
		var s:Vector2 = Vector2.ONE * pow(2,pow2)
		$vp.size = s
		$vp/bg.rect_size = s
		$vp/bg.rect_min_size = s
		$vp/layout.rect_size = s
		$vp/layout.rect_min_size = s

func do_generate(b:bool):
	generate = false
	if b:
		var bg:Panel = $vp/bg
		var styl:StyleBoxFlat = bg.get( "custom_styles/panel" )
		var msg:Label = $vp/layout/msg
		msg.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random('',true) )
		var fc = seeds.COLOR_GENERATOR.get_random()
		var bc = seeds.COLOR_GENERATOR.get_inverse(fc,1,0.8,0.8)
		msg.set( "custom_colors/font_color", fc )
		styl.bg_color = bc

func get_texture():
	#do_generate(true)
	var im = Image.new()
	im.copy_from( $vp.get_texture().get_data() )
	im.convert( Image.FORMAT_RGBA8 )
	var it = ImageTexture.new()
	it.create_from_image(im, Texture.FLAG_REPEAT & Texture.FLAG_REPEAT )
	return it

func request_push(n:Node):
	if !n.has_method( "push_texture" ):
		return
	if requestors.find(n) == -1:
		requestors.append(n)

func request_remove(n:Node):
	if requestors.find(n):
		 requestors.erase(n)

func _ready():
	do_apply_size(true)

func _process(delta):
	if !requestors.empty():
		do_generate(true)
		var r = requestors.pop_front()
		r.push_texture( get_texture() )
