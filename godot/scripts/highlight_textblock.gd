extends PanelContainer

signal highlight_clicked

onready var seeds = get_node("/root/seeds")

export (int,0,100) var margin:int = 15
export (float,0,10) var drag_multiply:float = 1
export (float,0,2) var width_min:float = 0.3
export (float,0,2) var width_max:float = 0.6

onready var font:Font = seeds.FONT_GENERATOR.get_random()
onready var bg_style:StyleBoxFlat = get( "custom_styles/panel" )
onready var bg_color:Color
onready var fg_color:Color
onready var vp_size:Vector2 = get_viewport().get_size()
onready var maxw:float = rand_range(width_min,width_max) * vp_size.x

var pager:Node = null
var scrollable:bool = false
var dragging:bool = false
var mouse_in:bool = false
var has_focus:bool = false
var pressed:bool = false
var layout_y_max:float = 0
var layout_y_target:float = 0
var layout_y_curr:float = 0

var mixer_target:float = 0
var mixer_current:float = 0
var multiplier_x_target:float = 0
var multiplier_x_current:float = 0
var speed_target:float = 0
var speed_current:float = 0
var sdr_time:float = 0

func purge():
	while $layout.get_child_count() > 0:
		$layout.remove_child( $layout.get_child(0) )
	rect_size.y = margin*3
	$layout.rect_size.y = margin

func _ready():
	# style
	bg_style = bg_style.duplicate()
	set( "custom_styles/panel", bg_style )
	bg_style.content_margin_bottom = margin
	bg_style.content_margin_left = margin
	bg_style.content_margin_right = margin
	bg_style.content_margin_top = margin
	reset_style()
	# shader
	multiplier_x_target = material.get_shader_param( "multiplier_x" )
	multiplier_x_current = multiplier_x_target
	material.set_shader_param( "mixer", mixer_current )
	# signals
	seeds.focus_register(self)
# warning-ignore:return_value_discarded
	connect("mouse_entered",self,"_on_mouse_entered")
# warning-ignore:return_value_discarded
	connect("mouse_exited",self,"_on_mouse_exited")
# warning-ignore:return_value_discarded
	get_viewport().connect("size_changed",self,"vp_changed")
	# reajusting to frame
	vp_changed()
	rect_size.x = rand_range(width_min,width_max) * vp_size.x
	rect_size.y = 0
	# vars
	layout_y_target = -rect_position.y + 10
	layout_y_curr = 0

func reset_style():
	bg_color = seeds.COLOR_GENERATOR.get_random()
	fg_color = seeds.COLOR_GENERATOR.get_inverse(bg_color,1,1,.3)
	material.set_shader_param( "dynamic_color0", bg_color )
	material.set_shader_param( "dynamic_color0", seeds.COLOR_GENERATOR.shift(bg_color,rand_range(0.35,0.55),0,0) )
	bg_style.bg_color = bg_color

func add_subtitle( s:String ):
	var lbl:Label = Label.new()
	lbl.autowrap = true
	lbl.text = s
	lbl.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random('subtitle') )
	lbl.set( "custom_colors/font_color", fg_color )
	$layout.add_child( lbl )
	lbl.mouse_filter = MOUSE_FILTER_IGNORE

func add_paragraph( s:String ):
	var lbl:Label = Label.new()
	lbl.autowrap = true
	lbl.text = s
	lbl.set( "custom_fonts/font", font )
	lbl.set( "custom_colors/font_color", fg_color )
	$layout.add_child( lbl )
	lbl.mouse_filter = MOUSE_FILTER_IGNORE

func vp_changed():
	vp_size = get_viewport().size
#	rect_size.y = vp_size.y
	if $layout.rect_size.y > vp_size.y:
		scrollable = true
		layout_y_max = (($layout.rect_size.y-vp_size.y) + (margin*2))*-1
	else:
		scrollable = false
		layout_y_max = margin

func _on_mouse_entered():
	mouse_in = true
	if $layout.rect_size.y > vp_size.y:
		scrollable = true
		layout_y_max = (($layout.rect_size.y-vp_size.y) + (margin*2))*-1
	else:
		scrollable = false
		layout_y_max = margin
	material.set_shader_param( "multiplier_y", rect_size.y * .1 )
	# trying to lock focus
	has_focus = seeds.focus_lock( self )
#	if has_focus:
#		hover(true)

func _on_mouse_exited():
	mouse_in = false
	if !dragging and has_focus:
#		hover(false)
		seeds.focus_unlock( self )
		has_focus = false

func focus_drag_start():
	if mouse_in && scrollable:
		dragging = true
		mixer_target = 0.75

func focus_drag_end():
	dragging = false
	mixer_target = 0
	multiplier_x_target = 0
	speed_target = 0

# warning-ignore:unused_argument
func focus_drag(amount:Vector2):
	if !scrollable:
		return
	pressed = false
	layout_y_target += amount.y * drag_multiply
	if amount.y < 0:
		multiplier_x_target = rect_size.x * .1
		speed_target = 30
	elif amount.y > 0:
		multiplier_x_target = -rect_size.x * .1
		speed_target = -30

func focus_lost():
	dragging = false

func focus_free():
	pass

func _process(delta):
	
	if rect_size.x > maxw:
		rect_size.x = maxw
		vp_changed()
	
	if !dragging:
		if layout_y_target > 10:
			layout_y_target = 10
		elif layout_y_target < layout_y_max:
			layout_y_target = layout_y_max
	
	if layout_y_curr != layout_y_target:
		var diff:float = layout_y_target-layout_y_curr
		layout_y_curr += diff*5*delta
		$layout.rect_position.y = layout_y_curr
		if abs(layout_y_target-layout_y_curr) < 1:
			layout_y_curr = layout_y_target
	
	if mixer_current != mixer_target:
		mixer_current += (mixer_target-mixer_current)*10*delta
		if abs(mixer_target-mixer_current) < 1e-3:
			mixer_current = mixer_target
		material.set_shader_param( "mixer", mixer_current )
	
	if multiplier_x_current != multiplier_x_target:
		multiplier_x_current += (multiplier_x_target-multiplier_x_current)*3*delta
		if abs(multiplier_x_target-multiplier_x_current) < 1e-3:
			multiplier_x_current = multiplier_x_target
		material.set_shader_param( "multiplier_x", multiplier_x_current )
	
	if speed_current != speed_target:
		speed_current += (speed_target-speed_current)*3*delta
		if abs(speed_current-multiplier_x_current) < 1e-3:
			speed_current = speed_target
	sdr_time += delta * speed_current
	material.set_shader_param( "time", sdr_time )

func _input(event):
	if !mouse_in:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			pressed = true
		else:
			if pressed:
				if pager != null:
					pager.hightlight_pressed(self)
				pressed = false
			dragging = false

func _exit_tree():
	seeds.focus_unregister(self)
