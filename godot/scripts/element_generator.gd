tool

extends Node2D

export (int,0,20) var number:int = 10
export (int,0,1500) var min_size:int = 50
export (int,0,1500) var max_size:int = 500

export (bool) var generate:bool = false setget do_generate

func do_generate(b:bool):
	generate = false
	if b:
		while get_child_count() > 0:
			remove_child(get_child(0))
		randomize()
		for i in range (0,number):
			var p:Panel = Panel.new()
			p.rect_size = Vector2( rand_range(min_size,max_size), rand_range(min_size,max_size) )
			var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
			norm_styl.bg_color = Color( rand_range(0,1),rand_range(0,1),rand_range(0,1) )
			p.set( "custom_styles/panel", norm_styl )
			add_child(p)
			p.owner = self.owner
