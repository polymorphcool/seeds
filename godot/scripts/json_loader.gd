tool

extends Node

signal data_changed

var json_path:String = ''
var data = null
var target:Node = null

func load_json( path:String ):
	json_path = path
	if json_path == '':
		return
	var f:File = File.new()
	f.open(json_path,File.READ)
	var jd = JSON.parse(f.get_as_text())
	if jd.result is Array:
		data = jd.result
	else:
		data = null
	f.close()
	emit_signal("data_changed")

func get_page(i:int):
	if data == null:
		return null
	for d in data:
		if d.index == i:
			return d
	return null
