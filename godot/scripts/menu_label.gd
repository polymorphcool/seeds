extends Label

signal mouse_in
signal mouse_out
signal element_pressed
signal element_clicked

var mouse_in:bool = false
var pressed:bool = false
var reactive:bool = true

func _ready():
	# signals
# warning-ignore:return_value_discarded
	self.connect("mouse_entered",self,"on_mouse_entered")
# warning-ignore:return_value_discarded
	self.connect("mouse_exited",self,"on_mouse_exited")

func on_mouse_entered():
	mouse_in = true
	pressed = false
	if !reactive:
		return

func on_mouse_exited():
	mouse_in = false
	if !reactive:
		return

func activate(b:bool):
	pressed = false

func _input(event):
	if !reactive or !mouse_in:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			emit_signal( "element_pressed", self )
			pressed = true
		elif pressed:
			emit_signal( "element_clicked", self )
			pressed = false
