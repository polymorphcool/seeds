extends Label

onready var seeds = get_node("/root/seeds")

export (float,0,2) var width_min:float = 0.3
export (float,0,2) var width_max:float = 0.6
export (float,0,2) var height_max:float = 0.6
export (float,0,10) var drag_multiply:float = 1

onready var fg_color:Color = seeds.COLOR_GENERATOR.get_random()
onready var bg_color:Color = seeds.COLOR_GENERATOR.get_inverse(fg_color,1,1,.3)
onready var ho_color:Color = seeds.COLOR_GENERATOR.shift(bg_color,.1,0,-.1)
onready var bg_style:StyleBoxFlat = get( "custom_styles/normal" )
onready var vp_size:Vector2 = get_viewport().size
onready var maxw:float = rand_range(width_min,width_max) * vp_size.x
onready var maxh:float = vp_size.y * height_max

var pager:Node = null
var dragging:bool = false
var mouse_in:bool = false
var has_focus:bool = false
var pressed:bool = false

var data = []

func set_pager( n:Node ):
	pager = n

func get_copy():
	var cp:Label = Label.new()
	cp.mouse_filter = Control.MOUSE_FILTER_IGNORE
	cp.set( "custom_styles/normal", bg_style )
	cp.rect_size = self.rect_size
	if cp.rect_size.y > maxh:
		cp.rect_size.y = maxh
	cp.autowrap = self.autowrap
	cp.clip_text = self.clip_text
	cp.text = self.text
	cp.set( "custom_fonts/font", self.get( "custom_fonts/font") )
	cp.set( "custom_colors/font_color", self.get( "custom_colors/font_color") )
	if $more.visible:
		var m:Panel = $more.duplicate()
		cp.add_child( m )
	return cp

func _ready():
	$more.visible = false
	bg_style = bg_style.duplicate()
	set( "custom_styles/normal", bg_style )
	reset_style()
	rect_size.x = maxw
	rect_size.y = 0
	# signals
	seeds.focus_register( self )
# warning-ignore:return_value_discarded
	connect("mouse_entered",self,"_on_mouse_entered")
# warning-ignore:return_value_discarded
	connect("mouse_exited",self,"_on_mouse_exited")

func _input(event):
	if !mouse_in or !has_focus:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			pressed = true
		elif pressed:
			if pager != null:
				pager.element_pressed(self)
			pressed = false

func _on_mouse_entered():
	mouse_in = true
	# trying to lock focus
	has_focus = seeds.focus_lock( self )
	if has_focus:
		hover(true)

func _on_mouse_exited():
	mouse_in = false
	if !dragging and has_focus:
		hover(false)
		seeds.focus_unlock( self )
		has_focus = false

func hover( b:bool ):
	if b:
#		material.set_shader_param( "hue_shift", 0 )
#		material.set_shader_param( "saturation_shift", 0 )
#		material.set_shader_param( "value_shift", 0 )
#		material.set_shader_param( "hue_shift_speed", 0 )
#		material.set_shader_param( "saturation_shift_speed", 0 )
#		material.set_shader_param( "value_shift_speed", 0 )
		if pager != null:
			pager.element_hover( self, true )
	else:
#		material.set_shader_param( "hue_shift", rand_range(0,1) )
#		material.set_shader_param( "value_shift_speed", rand_range(.15,.25) )
		if pager != null:
			pager.element_hover( self, false )

# focus callbacks
func focus_drag_start():
	dragging = true
	pressed = false
	if pager != null:
		pager.element_dragged( self, dragging )

func focus_drag_end():
	if dragging:
		dragging = false
		if pager != null:
			pager.element_dragged( self, dragging )

func focus_drag( amount:Vector2 ):
	rect_position += amount * drag_multiply

func focus_lost():
	dragging = false
	if has_focus and mouse_in:
		hover(false)

func focus_free():
	# trying to get focus back
	if mouse_in and !has_focus:
		_on_mouse_entered()

func _exit_tree():
	seeds.focus_unregister( self )

func reset_style():
	var f:Font = seeds.FONT_GENERATOR.get_random()
	self.set( "custom_fonts/font", f )
	self.set( "custom_colors/font_color", fg_color )
	bg_style.bg_color = bg_color
	$more.set( "custom_styles/panel", $more.get( "custom_styles/panel" ).duplicate() )
	$more.get( "custom_styles/panel" ).bg_color = seeds.COLOR_GENERATOR.get_inverse(fg_color,1,1,1)
	$more/cntr/lbl.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random() )
	$more/cntr/lbl.set( "custom_colors/font_color", fg_color )

func _process(delta):
	if rect_size.y > maxh:
		rect_size.y += ( maxh - rect_size.y ) * 30 * delta
		if abs( maxh - rect_size.y ) < 1:
			rect_size.y = maxh
		clip_text = true
		$more.visible = true
		$more.rect_size.x = rect_size.x
		$more/cntr.rect_size.x = rect_size.x
		$more.rect_size.y = $more/cntr.rect_size.y
		$more.rect_position.y = rect_size.y - $more.rect_size.y
