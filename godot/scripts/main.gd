extends Node2D

onready var seeds:Node = get_node("/root/seeds")
onready var vp_size:Vector2 = 	get_viewport().size

export (String) var url:String = ''
export (bool) var skip_timer:bool = false

var rng = RandomNumberGenerator.new()

func _ready():
	$json_loader.connect( "data_changed", self, "ready_to_start" )
	seeds.push_request( self, url, true )
	if !skip_timer:
		$Timer.start()
	var bgnd = Panel.new()
	bgnd.rect_position.x = 0
	bgnd.rect_position.y = 0
	bgnd.rect_size = vp_size
	splash_layout()
	var loading_label = $splash_container/loading
	loading_label.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random('subtitle') )
	var fc = seeds.COLOR_GENERATOR.get_random()
	loading_label.set( "custom_colors/font_color", fc )
	loading_label.rect_size.x = vp_size.x
	loading_label.rect_size.y = vp_size.y
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = Color(0, 0, 0, 0)
	norm_styl.content_margin_top = vp_size.y/4
	norm_styl.content_margin_right = vp_size.x/4
	norm_styl.content_margin_bottom = vp_size.y/4
	norm_styl.content_margin_left = vp_size.x/4
	loading_label.set( "custom_styles/normal", norm_styl )
	loading_label.text = "Loading text..."
	loading_label.align = 1
	loading_label.valign = 1

func _on_request_success( cache_path:String ):
	$json_loader.load_json( cache_path )

func _on_request_failed( cache_path:String ):
	print( "fuck! no internet?? cache: " + cache_path )
	if cache_path != "":
		$json_loader.load_json( cache_path )

func _exit_tree():
	seeds.remove_request( self )

func splash_layout():
	$splash_container.rect_position.x = 0
	$splash_container.rect_position.y = 0
	$splash_container.rect_size.x = 0
	$splash_container.rect_size.y = 0
		
func seed_layout():
	var seeds_title:Label = Label.new()
	seeds_title.autowrap = false
	seeds_title.text = 'Seeds'
	seeds_title.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random('title') )
	var fc = seeds.COLOR_GENERATOR.get_random()
	seeds_title.set( "custom_colors/font_color", fc )
	seeds_title.rect_size.x = vp_size.x
	seeds_title.rect_size.y = vp_size.y
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = Color(0,0,0,0)
	norm_styl.content_margin_top = -vp_size.y/4
	norm_styl.content_margin_right = vp_size.x/4
	norm_styl.content_margin_bottom = vp_size.y/4
	norm_styl.content_margin_left = vp_size.x/4
	seeds_title.set( "custom_styles/normal", norm_styl )
	seeds_title.align = 1
	seeds_title.valign = 1
	$splash_container.add_child( seeds_title )

func _on_Timer_timeout():
	var s:Node = $splash_container.get_child(0)
	$splash_container.remove_child(s)
	s.queue_free()
	$menu.generate()

func ready_to_start():
	print($Timer.time_left)
	if skip_timer or $Timer.time_left < 0.2:
		print( "menu generate" )
		$menu.generate()
	
func _process(delta):
	if $Timer.time_left > 0:
		seed_layout()
		#return
