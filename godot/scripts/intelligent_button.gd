extends Button

signal button_pressed

func _ready():
	self.connect("pressed",self,"send_button_pressed")

func send_button_pressed():
	emit_signal( "button_pressed", self )
