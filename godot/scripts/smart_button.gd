extends Panel

# warning-ignore:unused_signal
signal pressed_start
# warning-ignore:unused_signal
signal pressed_end
signal mouse_in
signal mouse_out
signal button_pressed

export (String) var text:String = 'smart button' setget set_text
export (bool) var show_counter:bool = false
export (float,0,100) var sdr_dyn_speed_enter:float = 30
export (float,0,100) var sdr_dyn_speed_exit:float = 10
export (float,0,100) var dot_min:float = 2
export (float,0,100) var dot_max:float = 10

onready var seeds:Node = get_node("/root/seeds")
# making bg_style unique
onready var bg_style:StyleBoxFlat = null
onready var bg_mat:ShaderMaterial = null
onready var container:HBoxContainer = $container
onready var counter:Label = $container/counter
onready var txt:Label = $container/txt

var shader01 = load('res://materials/smart_button01.material')
var shader02 = load('res://materials/smart_button.material')
var shader03 = load('res://materials/ocean.material')

var is_ready:bool = false
var click_count:int = 0
var hover:bool = false
var pressed:bool = false
var reactive:bool = true
var shader_name

# shader fx
onready var sdr_dyn_speed:float = sdr_dyn_speed_enter
var sdr_dyn_target:float = 0
var sdr_dyn_current:float = 0

func set_text(s:String):
	text = s
	if is_ready:
		txt.text = text

func _ready():
	
	is_ready = true
	set_text(text)
	
	bg_style = get( "custom_styles/panel" ).duplicate()
	set( "custom_styles/panel", bg_style )
	
	# shader material
	var shaders = [shader01, shader02, shader03]
	shader_name = shaders[rand_range(0,3)]
	material = shader_name
	bg_mat = material.duplicate()
	material = bg_mat
	bg_mat.set_shader_param( "dynamic", sdr_dyn_current )
	
	
	# signals
# warning-ignore:return_value_discarded
	self.connect("mouse_entered",self,"on_mouse_entered")
# warning-ignore:return_value_discarded
	self.connect("mouse_exited",self,"on_mouse_exited")
	
	# setting fonts for labels
	randomise_font()
	
	# fixing sizes
	counter.rect_min_size.x = counter.rect_size.x
	rect_min_size = container.rect_size
	
	# preparing display
	counter.visible = show_counter
	reset_color()
	update_click_count()
	return shader_name

func activate(b:bool):
	if !b and hover:
		on_mouse_exited()
	reactive = b

func randomise_font():
	var f:Font = seeds.FONT_GENERATOR.get_random()
	txt.set( "custom_fonts/font", f )
	counter.set( "custom_fonts/font", f )

func on_mouse_entered():
	if !reactive:
		return
	emit_signal("mouse_in", self)
	randomise_font()
	hover = true
	if material == null:
		material = shader_name
		bg_mat = material.duplicate()
		material = bg_mat
		bg_mat.set_shader_param( "dynamic", sdr_dyn_current )
	# avoiding jumps of configuration
	if sdr_dyn_current == 0:
		var c:Color = seeds.COLOR_GENERATOR.get_random()
		bg_mat.set_shader_param( "dynamic_color0", c )
		bg_mat.set_shader_param( "dynamic_color1", seeds.COLOR_GENERATOR.shift( c, rand_range(0.1,0.7),0,0 ) )
		if material == shader01:
			#shader_name = 'cineshader_test_wlVGWd.shader'
			bg_mat.set_shader_param( "uv_mult", Vector2( rand_range(0.001,2), rand_range(0.1,1) ) )
			bg_mat.set_shader_param( "screen_ratio", rect_size.y/rect_size.x )
			bg_mat.set_shader_param( "lentille_strength", rand_range(10,100) )
		elif material == shader02:
			bg_mat.set_shader_param( "uv_mult", Vector2( rand_range(0.001,2), rand_range(0.1,1) ) )
			bg_mat.set_shader_param( "screen_ratio", rect_size.y/rect_size.x )
			bg_mat.set_shader_param( "lentille_strength", rand_range(10,100) )
		else:
			bg_mat.set_shader_param( "uv_mult", Vector2( rand_range(0.001,2), rand_range(0.1,1) ) )
			bg_mat.set_shader_param( "screen_ratio", rect_size.y/rect_size.x )
			bg_mat.set_shader_param( "lentille_strength", rand_range(10,100) )
		
	sdr_dyn_speed = sdr_dyn_speed_enter
	sdr_dyn_target = 1

func on_mouse_exited():
	if !reactive:
		return
	emit_signal("mouse_out", self)
	randomise_font()
	hover = false
	sdr_dyn_speed = sdr_dyn_speed_exit
	sdr_dyn_target = 0
	reset_color()

func reset_color():
	var fc:Color = seeds.COLOR_GENERATOR.get_random()
	var cc:Color =  seeds.COLOR_GENERATOR.get_inverse(fc,1,0.9,0.4)
	var bc:Color =  seeds.COLOR_GENERATOR.get_inverse(fc,1,0.9,0.8)
	txt.set( "custom_colors/font_color", fc )
	counter.set( "custom_colors/font_color", cc )
	bg_mat.set_shader_param( "static_color", bc )
	bg_style.set_bg_color(bc)

func update_click_count():
	counter.text = str( click_count )

func _input(event):
	
	if !reactive or !hover:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			pressed = true
			click_count += 1
			update_click_count()
		elif pressed:
			emit_signal( "button_pressed", self )

func _process(delta):
	if sdr_dyn_current != sdr_dyn_target:
		sdr_dyn_current += (sdr_dyn_target-sdr_dyn_current) *  min(1, delta * sdr_dyn_speed)
		if abs(sdr_dyn_target-sdr_dyn_current) < 1e-3:
			sdr_dyn_current = sdr_dyn_target
			if sdr_dyn_target == 0:
				material = null
		bg_mat.set_shader_param( "dynamic", sdr_dyn_current )
	if hover:
		if shader_name == shader02:
			var mp:Vector2 = get_viewport().get_mouse_position() - rect_global_position
			mp /= rect_size
			var uvo:Vector2 = bg_mat.get_shader_param( "uv_offset" )
			uvo += ( mp * 50 - uvo ) * 0.3 * delta
			bg_mat.set_shader_param( "uv_offset", uvo )
			var uvc:Vector2 = bg_mat.get_shader_param( "uv_center" )
			uvc += ( Vector2(0.5-(mp.x-0.5)*0.3, mp.y) - uvc ) * 3 * delta
			bg_mat.set_shader_param( "uv_center", uvc )
		elif shader_name == shader01:
			var mp:Vector2 = get_viewport().get_mouse_position() - rect_global_position
			mp /= rect_size
			#var uvo:Vector2 = bg_mat.get_shader_param( "uv_offset" )
			#uvo += ( mp * 50 - uvo ) * 0.3 * delta
			#bg_mat.set_shader_param( "uv_offset", uvo )
			#var uvc:Vector2 = bg_mat.get_shader_param( "uv_center" )
			#uvc += ( Vector2(0.5-(mp.x-0.5)*0.3, mp.y) - uvc ) * 3 * delta
			#bg_mat.set_shader_param( "uv_center", uvc )
		else:
			var mp:Vector2 = get_viewport().get_mouse_position() - rect_global_position
			mp /= rect_size
			#var uvo:Vector2 = bg_mat.get_shader_param( "uv_offset" )
			#uvo += ( mp * 50 - uvo ) * 0.3 * delta
			#bg_mat.set_shader_param( "uv_offset", uvo )
			#var uvc:Vector2 = bg_mat.get_shader_param( "uv_center" )
			#uvc += ( Vector2(0.5-(mp.x-0.5)*0.3, mp.y) - uvc ) * 3 * delta
			#bg_mat.set_shader_param( "uv_center", uvc )
			
