extends TextureRect

onready var seeds = get_node("/root/seeds")

export (String) var url:String = ''
export (Vector2) var max_size:Vector2 = Vector2(500,500)
# set to false to force re-downloading the url even if cache exists
export (float,0,10) var drag_multiply:float = 1
export (bool) var use_cache:bool = true

var pager:Node = null
var dragging:bool = false
var mouse_in:bool = false
var has_focus:bool = false
var pressed:bool = false

func get( s:String ):
	url = s
	$loading/container/msg.text = "LOADING"
	seeds.push_request( self, url, !use_cache )

func set_pager( n:Node ):
	pager = n

func _ready():
	# making uniques
	material = material.duplicate()
	$loading.material = $loading.material.duplicate()
	# messages
	$loading/container/msg.text = "URL?"
	# random design!!!
	# fx
	material.set_shader_param( "hue_shift", rand_range(0,1) )
	# controls
	var fc:Color = seeds.COLOR_GENERATOR.get_random()
	$loading/container/msg.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random() )
	$loading/container/msg.set( "custom_colors/font_color", fc )
	$loading.material.set_shader_param( "dynamic_color0", seeds.COLOR_GENERATOR.shift( fc, rand_range(.1,.3), 0,0) )
	$loading.material.set_shader_param( "dynamic_color1", seeds.COLOR_GENERATOR.shift( fc, rand_range(.4,.7), 0,0) )
	$loading.material.set_shader_param( "frequency", rand_range(15,25) )
	$loading.material.set_shader_param( "speed", rand_range(1,5) )
	$loading.material.set_shader_param( "radius", rand_range(1,4) )
	# signals
	seeds.focus_register( self )
	connect("mouse_entered",self,"_on_mouse_entered")
	connect("mouse_exited",self,"_on_mouse_exited")

func _input(event):
	if !mouse_in or !has_focus:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			pressed = true
		elif pressed:
			if pager != null:
				pager.element_pressed(self)
			pressed = false

func _on_mouse_entered():
	mouse_in = true
	# trying to lock focus
	has_focus = seeds.focus_lock( self )
	if has_focus:
		hover(true)

func _on_mouse_exited():
	mouse_in = false
	if !dragging and has_focus:
		hover(false)
		seeds.focus_unlock( self )
		has_focus = false

func hover( b:bool ):
	if b:
		material.set_shader_param( "hue_shift", 0 )
		material.set_shader_param( "saturation_shift", 0 )
		material.set_shader_param( "value_shift", 0 )
		material.set_shader_param( "hue_shift_speed", 0 )
		material.set_shader_param( "saturation_shift_speed", 0 )
		material.set_shader_param( "value_shift_speed", 0 )
		if pager != null:
			pager.element_hover( self, true )
	else:
		material.set_shader_param( "hue_shift", rand_range(0,1) )
		material.set_shader_param( "value_shift_speed", rand_range(.15,.25) )
		if pager != null:
			pager.element_hover( self, false )

# focus callbacks
func focus_drag_start():
	dragging = true
	pressed = false
	if pager != null:
		pager.element_dragged( self, dragging )

func focus_drag_end():
	if dragging:
		dragging = false
		if pager != null:
			pager.element_dragged( self, dragging )

func focus_drag( amount:Vector2 ):
	rect_position += amount * drag_multiply

func focus_lost():
	dragging = false
	if has_focus and mouse_in:
		hover(false)

func focus_free():
	# trying to get focus back
	if mouse_in and !has_focus:
		_on_mouse_entered()

func _exit_tree():
	seeds.focus_unregister( self )

func _on_request_success( cache_path:String ):
	texture = ImageTexture.new()
	texture.load( cache_path )
	rect_size = texture.get_size()
	if rect_size.x > max_size.x:
		rect_size.y /= rect_size.x / max_size.x
		rect_size.x = max_size.x
	if rect_size.y > max_size.y:
		rect_size.x /= rect_size.y / max_size.y
		rect_size.y = max_size.y
	# removing material to save process
	$loading.material = null
	$loading.visible = false
	material.set_shader_param( "value_shift_speed", rand_range(.15,.25) )
	if pager != null:
		pager.element_resize( self )

func _on_request_failed( cache_path:String ):
	$loading.material.set_shader_param( "speed", rand_range(.1,.3) )
	$loading/container/msg.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random() )
	$loading/container/msg.text = "FAILED..."
