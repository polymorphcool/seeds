tool

extends Line2D

export (float,0,1000) var radius:float = 50 setget set_radius
export (int,2,300) var definition:int = 32 setget set_definition
export (bool) var generate:bool = false setget do_generate

func set_radius(f:float):
	radius = f
	refresh()

func set_definition(i:int):
	definition = i
	refresh()
	
func do_generate(b:bool):
	generate = false
	if b:
		refresh()

func refresh():
	var pts:PoolVector2Array = PoolVector2Array()
	var agap:float = TAU / (definition-1)
	for i in range(0,definition):
		var a:float = agap*i
		pts.append( Vector2(cos(a),sin(a)) * radius )
	points = pts
