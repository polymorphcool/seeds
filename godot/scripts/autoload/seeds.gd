extends Node

# path
const IMG_CACHE_FOLDER:String = 	'user://images/cache/'
const IMG_EXTENSIONS:Array = 		['.png','.jpg','.jpeg','.gif']
const JSON_CACHE_FOLDER:String = 	'user://json/cache/'

# values
const MOUSE_DRAG_RADIUS:int = 		10
const MOUSE_BORDER:int = 			50

# generators
var FONT_GENERATOR = 				preload( 'res://objects/font_generator.tscn' ).instance()
var COLOR_GENERATOR = 				preload( 'res://objects/color_generator.tscn' ).instance()

# templates
const MENU_LABEL = 					preload( 'res://objects/menu_label.tscn' )
const MENU_BUTTON = 				preload( 'res://objects/menu_button.tscn' )
const PAGER_IMAGE = 				preload( 'res://objects/pager_image.tscn' )
const PAGER_TEXT = 					preload( 'res://objects/pager_textblock.tscn' )
const SMART_HTTPREQUEST = 			preload( 'res://objects/smart_httprequest.tscn' )
const HIGHTLIGHT_IMAGE = 			preload( 'res://objects/highlight_image.tscn' )
const HIGHTLIGHT_TEXT = 			preload( 'res://objects/hightlight_textblock.tscn' )

# shaders
var SHADER01 = preload('res://materials/smart_button01.material')
var SHADER02 = preload('res://materials/smart_button.material')
var SHADER03 = preload('res://materials/ocean.material')

# functions
func check_cache_folders():
	var d:Directory = Directory.new()
	if !d.dir_exists( IMG_CACHE_FOLDER ):
# warning-ignore:return_value_discarded
		d.make_dir_recursive( IMG_CACHE_FOLDER )
	if !d.dir_exists( JSON_CACHE_FOLDER ):
# warning-ignore:return_value_discarded
		d.make_dir_recursive( JSON_CACHE_FOLDER )

#############################################################################################################
########################################## HTTP REQUESTS ####################################################
#############################################################################################################
# http request handling
var requests:Array = []

func register_request( requestor:Node, url:String, force:bool ):
	
	# validation that requestor has he right methods
	if not ( requestor.has_method( "_on_request_success" ) and requestor.has_method( "_on_request_failed" ) ):
		printerr( requestor, " must have methods _on_request_success(cache_path:String) &  _on_request_failed(cache_path:String)!" )
		printerr( "Request cancelled" )
		return
	
	# selection of cache folder
	var cache_folder:String = ''
	var lw_url:String = url.to_lower()
	# removal of url arguments
	if lw_url.find('?') > -1:
		lw_url = lw_url.substr(0,lw_url.find('?'))
	if lw_url.ends_with( ".json" ):
		cache_folder = JSON_CACHE_FOLDER
	for ext in IMG_EXTENSIONS:
		if lw_url.ends_with( ext ):
			cache_folder = IMG_CACHE_FOLDER
	if cache_folder == '':
		printerr( "No cache folder for this file type!", url )
		printerr( "Request cancelled" )
		return
	
	# object creation
	var ir:HTTPRequest = SMART_HTTPREQUEST.instance()
	var new_request:Dictionary = {
		'requestor': requestor,
		'httprequest': ir,
		'url': url
	}
# warning-ignore:return_value_discarded
	ir.connect( "request_success", self, 'http_request_success' )
# warning-ignore:return_value_discarded
	ir.connect( "request_failed", self, 'http_request_failed' )
	add_child( ir )
	# storing request
	requests.append( new_request )
	# and starting the request
	ir.send( url, cache_folder, force )

func http_request_success( req:HTTPRequest ):
	print( "success! ", req.cache_path )
	var r = find_request_by_httprequest( req )
	if r != null:
		r.requestor._on_request_success( req.cache_path )
	remove_request( req )

func http_request_failed( req:HTTPRequest ):
	print( "failed! ", req.url )
	var r = find_request_by_httprequest( req )
	if r != null:
		# invalidating cache_path if it not exists
		if !req.cache_exists:
			req.cache_path = ''
		r.requestor._on_request_failed( req.cache_path )
	remove_request( req )

func find_request_by_requestor( n:Node ):
	for r in requests:
		if r.requestor == n:
			return r
	return null

func find_request_by_httprequest( ir:HTTPRequest ):
	for r in requests:
		if r.httprequest == ir:
			return r
	return null

func push_request( requestor:Node, url:String, force:bool = false ):
	var r = find_request_by_requestor( requestor )
	if r != null:
		remove_request( r )
	register_request( requestor, url, force )

func remove_request( variant ):
	var r = null
	if variant is Dictionary:
		http_request_failed( variant.httprequest )
		r = variant
	elif variant is HTTPRequest:
		r = find_request_by_httprequest( variant )
	elif variant is Node:
		r = find_request_by_requestor( variant )
	else:
		printerr( "remove_request, wtf is this argument???: ", variant )
	if r != null:
		# just to be sure
		r.httprequest.cancel_request()
		requests.erase( r )

#############################################################################################################
######################################## FOCUS MANAGEMENT ###################################################
#############################################################################################################

var focus_node:Node = null
var focus_listener:Array = []

func focus_register( n:Node ):
	if !(
		n.has_method("focus_drag_start") and 
		n.has_method("focus_drag_end") and 
		n.has_method("focus_drag") and 
		n.has_method("focus_lost") and 
		n.has_method("focus_free")
		):
		printerr( "To register as a focus listener, node MUST implement these methods:" )
		printerr( "\t"+"focus_drag_start()" )
		printerr( "\t"+"focus_drag_end()" )
		printerr( "\t"+"focus_drag(amount:Vector2)" )
		printerr( "\t"+"focus_lost()" )
		printerr( "\t"+"focus_free()" )
	if focus_listener.find( n ) == -1:
		focus_listener.append( n )

func focus_unregister( n:Node ):
	if focus_listener.find( n ) > -1:
		focus_listener.erase( n )
	if focus_node == n:
		focus_unlock( n )

func focus_lock( n:Node ):
	if focus_listener.find( n ) == -1:
#		printerr( "focus_lock :: Node must be registered as a focus listener (call focus_register(n:Node))" )
		return false
	if focus_node != null and focus_node != n:
		return false
	for fl in focus_listener:
		if fl != n:
			fl.focus_lost()
	focus_node = n
	return true

# warning-ignore:unused_argument
func focus_unlock( n:Node ):
	if focus_node != null and focus_node != n:
#		printerr( "focus_unlock :: focus is already locked!" )
		return
	if focus_listener.find( n ) == -1:
#		printerr( "focus_unlock :: Node must be registered as a focus listener (call focus_register(n:Node))" )
		return
	for fl in focus_listener:
		fl.focus_free()
	focus_node = null

func focus_drag_start():
	if focus_node != null:
		focus_node.focus_drag_start()

func focus_drag_end():
	if focus_node != null:
		focus_node.focus_drag_end()

func focus_drag( mp:Vector2 ):
	if focus_node != null:
		focus_node.focus_drag( mp )

#############################################################################################################
######################################## MOUSE MANAGEMENT ###################################################
#############################################################################################################

var mouse_start:Vector2 = Vector2.INF
var mouse_previous:Vector2 = Vector2.INF
var mouse_dragged:bool = false
var mouse_pressed:bool = false

#############################################################################################################
########################################## STD FUNCTIONS ####################################################
#############################################################################################################

func _ready():
	check_cache_folders()
	add_child(FONT_GENERATOR)
	add_child(COLOR_GENERATOR)

# warning-ignore:unused_argument
func _process(delta):
	
	# monitoring of active requests
	var http_zombies:Array = []
	var res = 0
	for r in requests:
		res = r.httprequest.get_http_client_status()
		match res:
			HTTPClient.STATUS_DISCONNECTED:
				http_zombies.append( r )
			HTTPClient.STATUS_CANT_RESOLVE:
				http_zombies.append( r )
			HTTPClient.STATUS_CANT_CONNECT:
				http_zombies.append( r )
			HTTPClient.STATUS_CONNECTION_ERROR:
				http_zombies.append( r )
			HTTPClient.STATUS_SSL_HANDSHAKE_ERROR:
				http_zombies.append( r )
	if !http_zombies.empty():
		for z in http_zombies:
			remove_request(z)

func _input(event):
	
	if event is InputEventMouseMotion and mouse_pressed:
		if !mouse_dragged:
			if (event.position-mouse_start).length() > MOUSE_DRAG_RADIUS:
				focus_drag_start()
				mouse_dragged = true
				mouse_previous = event.position
		else:
			var delta:Vector2 = event.position - mouse_previous
			# sending delta to focus node
			focus_drag( delta )
			mouse_previous = event.position
	
	elif event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		mouse_pressed = event.pressed
		if !mouse_pressed:
			# releasing drag
			if mouse_dragged:
				focus_drag_end()
				mouse_dragged = false
		else:
			mouse_previous = event.position
			mouse_start = event.position
	
