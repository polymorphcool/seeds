extends HTTPRequest

signal request_success
signal request_failed

var url:String = ''
var cache_path:String = ''
var cache_exists:bool = false
var active_request:bool = false

func _ready():
	self.connect("request_completed",self,"_http_request_completed")

func send( url:String, cache_folder:String, force:bool = false ):
	self.url = url
	cache_path = cache_folder + url.substr(url.find_last('//')+2).replace('/','')
	var f:File = File.new()
	cache_exists = f.file_exists( cache_path )
	f.close()
	if !force and cache_exists:
		emit_signal( "request_success", self )
		f.close()
		return
	download_file = cache_path
	var error = request(url)
	if error != OK:
		emit_signal( "request_failed", self )
	else:
		active_request = true
	

func _http_request_completed(result,response_code,headers,body):
	active_request = false
	if cache_path != '' and response_code != 200:
		emit_signal( "request_failed", self )
		return
	emit_signal( "request_success", self )
	

