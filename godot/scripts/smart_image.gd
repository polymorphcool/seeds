extends TextureRect

signal control_size_changed
signal image_drag_start
signal image_drag_stop
signal image_pressed_start
signal image_pressed_stop

onready var seeds = get_node("/root/seeds")

export (String) var url:String = ''
export (Vector2) var max_size:Vector2 = Vector2(500,500)
# set to false to force re-downloading the url even if cache exists
export (float,0,10) var drag_multiply:float = 1
export (bool) var use_cache:bool = true

var enabled:bool = true
var dragging:bool = false
var mouse_inside:bool = false

func enable( b:bool ):
	enabled = b
	if !enabled:
		dragging = false
		mouse_inside = false
		seeds.focus_unlock( self )

func _ready():
	# making uniques
	material = material.duplicate()
	$loading.material = $loading.material.duplicate()
	# messages
	$loading/container/msg.text = "URL?"
	# random design!!!
	# fx
	material.set_shader_param( "hue_shift", rand_range(0,1) )
	# controls
	var fc:Color = seeds.COLOR_GENERATOR.get_random()
	$loading/container/msg.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random() )
	$loading/container/msg.set( "custom_colors/font_color", fc )
	$loading.material.set_shader_param( "dynamic_color0", seeds.COLOR_GENERATOR.shift( fc, rand_range(.1,.3), 0,0) )
	$loading.material.set_shader_param( "dynamic_color1", seeds.COLOR_GENERATOR.shift( fc, rand_range(.4,.7), 0,0) )
	$loading.material.set_shader_param( "frequency", rand_range(15,25) )
	$loading.material.set_shader_param( "speed", rand_range(1,5) )
	$loading.material.set_shader_param( "radius", rand_range(1,4) )
	
	connections(true)

func connections(b:bool):
	if b:
		# register as focus listener
		seeds.focus_register( self )
		# connecting signals
		connect("mouse_entered",self,"_on_mouse_entered")
		connect("mouse_exited",self,"_on_mouse_exited")
	else:
		seeds.focus_unregister( self )
		disconnect("mouse_entered",self,"_on_mouse_entered")
		disconnect("mouse_exited",self,"_on_mouse_exited")

func get( s:String ):
	url = s
	$loading/container/msg.text = "LOADING"
	seeds.push_request( self, url, !use_cache )

func _input(event):
	if !mouse_inside or !enabled:
		return
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		if event.pressed:
			emit_signal( "image_pressed_start", self )
		else:
			emit_signal( "image_pressed_stop", self )

func _on_request_success( cache_path:String ):
	texture = ImageTexture.new()
	texture.load( cache_path )
	rect_size = texture.get_size()
	if rect_size.x > max_size.x:
		rect_size.y /= rect_size.x / max_size.x
		rect_size.x = max_size.x
	if rect_size.y > max_size.y:
		rect_size.x /= rect_size.y / max_size.y
		rect_size.y = max_size.y
	# removing material to save process
	$loading.material = null
	$loading.visible = false
	material.set_shader_param( "value_shift_speed", rand_range(.15,.25) )
	emit_signal( "control_size_changed", self )

func _on_request_failed( cache_path:String ):
	$loading.material.set_shader_param( "speed", rand_range(.1,.3) )
	$loading/container/msg.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random() )
	$loading/container/msg.text = "FAILED..."

func _on_mouse_entered():
	if !enabled:
		return
	mouse_inside = true
	material.set_shader_param( "hue_shift", 0 )
	material.set_shader_param( "saturation_shift", 0 )
	material.set_shader_param( "value_shift", 0 )
	material.set_shader_param( "hue_shift_speed", 0 )
	material.set_shader_param( "saturation_shift_speed", 0 )
	material.set_shader_param( "value_shift_speed", 0 )
	focus_lock()

func _on_mouse_exited():
	if !enabled:
		return
	mouse_inside = false
	material.set_shader_param( "hue_shift", rand_range(0,1) )
	material.set_shader_param( "value_shift_speed", rand_range(.15,.25) )
	focus_unlock()

func focus_lock():
	seeds.focus_lock( self )

func focus_unlock():
	if dragging:
		seeds.focus_unlock( self )

# focus callbacks
func focus_drag_start():
	pass

func focus_drag_end():
	if dragging:
		dragging = false
		emit_signal( "image_drag_stop", self )

func focus_drag( amount:Vector2 ):
	if !dragging:
		dragging = true
		emit_signal( "image_drag_start", self )
	rect_position += amount * drag_multiply

func focus_lost():
	dragging = false

func _exit_tree():
	connections(false)
