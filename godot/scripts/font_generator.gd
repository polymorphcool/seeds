tool

extends Node

const font_folder:String = "res://fonts/"
const font_json:String = "res://fonts/lib.json"
const folder_skipped:Array = ["res://fonts/icons/"]
const default_usage:String = 'bodytext'

# check this and run the game (F5) one time to create font_json
# for release, UNCHECK THIS!
export (bool) var generate_json:bool = true
export (String) var font_extensions:String = ".otf,.ttf"
export (int,1,100) var body_min_size:int = 18
export (int,1,100) var body_max_size:int = 20
export (int,1,100) var title_min_size:int = 24
export (int,1,100) var title_max_size:int = 28
export (int,1,100) var subtitle_min_size:int = 24
export (int,1,100) var subtitle_max_size:int = 28

var extensions:PoolStringArray = []
var font_usage:Dictionary = {}
var font_path:Array = []
var font_data:Array = []

func find_font( name:String, size:int ):
	for f in font_data:
		if f.name == name and f.size == size:
			return f
	return null

func load_folder( path:String ):
	if path in folder_skipped:
		return
	var folder:Directory = Directory.new()
	var d:Directory = Directory.new()
	if folder.open(path) == OK:
# warning-ignore:return_value_discarded
		folder.list_dir_begin()
		var file:String = folder.get_next()
		while (file != ""):
			if file == '..' or file == '.':
				file = folder.get_next()
				continue
			if d.file_exists( path + file ):
				for e in extensions:
					var h:PoolStringArray = (path + file).substr(len(font_folder)).rsplit( '/', false )
					if file.ends_with( e ):
						var ff:String = ''
						if h.size() > 1:
							ff = h[ len(h)-2 ]
						font_path.append( {
							'path': 	path + file,
							'folder':	ff,
							'name': 	file.replace(e,'').to_lower() 
							} )
						break
			elif d.dir_exists( path + file ):
				load_folder( path + file + '/' )
			file = folder.get_next()

func generate():
	
	font_path = []
	font_data = []
	font_usage = {
		default_usage : [],
		'title' : [],
		'subtitle' : []
	}
	
	if generate_json:
		
		load_folder( font_folder )
		# generation of font
		for f in font_path:
			var fd = load(f.path)
			for size in range( body_min_size, body_max_size ):
				var dynf = DynamicFont.new()
				dynf.font_data = fd
				dynf.size = size
				var usage:Array = [default_usage]
				var fdata = {
					"path": 	f.path,
					"usage": 	usage,
					"name": 	f.name,
					"size": 	size,
					"font": 	dynf
				}
				font_data.append( fdata )
				font_usage.bodytext.append( fdata )
		for f in font_path:
			var fd = load(f.path)
			for size in range( subtitle_min_size, subtitle_max_size ):
				var fdata = find_font( f.name, size )
				if fdata != null:
					if not 'subtitle' in fdata.usage:
						fdata.usage.append( 'subtitle' )
					font_usage.subtitle.append( fdata )
					continue
				var dynf = DynamicFont.new()
				dynf.font_data = fd
				dynf.size = size
				var usage:Array = ["subtitle"]
				fdata = {
					"path": 	f.path,
					"usage": 	usage,
					"name": 	f.name,
					"size": 	size,
					"font": 	dynf
				}
				font_data.append( fdata )
				font_usage.subtitle.append( fdata )
		for f in font_path:
			if f.folder != 'title':
				continue
			var fd = load(f.path)
			for size in range( title_min_size, title_max_size ):
				var fdata = find_font( f.name, size )
				if fdata != null:
					if not 'title' in fdata.usage:
						fdata.usage.append( 'title' )
					font_usage.title.append( fdata )
					continue
				var dynf = DynamicFont.new()
				dynf.font_data = fd
				dynf.size = size
				var usage:Array = ["title"]
				fdata = {
					"path": 	f.path,
					"usage": 	usage,
					"name": 	f.name,
					"size": 	size,
					"font": 	dynf
				}
				font_data.append( fdata )
				font_usage.title.append( fdata )
		var json_dict = []
		for fd in font_data:
			json_dict.append( {
				"path": 	fd.path,
				"usage": 	fd.usage,
				"name": 	fd.name,
				"size": 	fd.size
			} )
		font_data.sort_custom(self, "font_data_compare")
		var f:File = File.new()
		if f.open( font_json, File.WRITE ) == OK:
			f.store_string(to_json(json_dict))
			print( font_json, " successfully saved" )
		f.close()
	
	else:
		# loading the lib!
		var f:File = File.new()
# warning-ignore:return_value_discarded
		f.open(font_json,File.READ)
		var jdata = JSON.parse(f.get_as_text()).result
		f.close()
		# loading all fonts
		var fts = {}
		for fd in jdata:
			if not fd.name in fts:
				fts[fd.name] = load(fd.path)
		for fd in jdata:
			var dynf = DynamicFont.new()
			dynf.font_data = fts[fd.name]
			dynf.size = int(fd.size)
			var fdata = {
				"path": 	fd.path,
				"usage": 	fd.usage,
				"name": 	fd.name,
				"size": 	fd.size,
				"font": 	dynf
			}
			font_data.append( fdata )
			for u in fd.usage:
				if not u in font_usage:
					printerr( "WRONG USAGE! ", u ) 
					continue
				font_usage[u].append( fdata )
	
	print( "font_generator: " + str(font_data.size()) + " dynamic fonts generated" )
	print( "\t" + str(font_path.size()) + " differents fonts" )
	print( "\t" + "size from " + str(body_min_size) + " to " + str(body_max_size) )

func font_data_compare(a, b):
	if a.name == b.name:
		return a.size < b.size
	return a.name < b.name

func get_random( usage:String = '', return_font:bool = true ):
	if !usage in font_usage:
		usage = default_usage
	randomize()
	var subset:Array = []
	for k in font_usage.keys():
		if k == usage:
			subset = font_usage[k]
	if subset.empty():
		printerr( "something went awfully wrong in font generation: NO font found for ", usage )
		return null
	var fdl:int = subset.size()
# warning-ignore:narrowing_conversion
	var i:int = rand_range(0,fdl)
	while i >= fdl:
# warning-ignore:narrowing_conversion
		i = rand_range(0,fdl)
	if return_font:
		return subset[i].font
	else:
		return subset[i]

func _ready():
	extensions = font_extensions.rsplit( ',', false )
	generate()
