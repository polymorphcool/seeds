tool

extends Node

export (float,0,1) var hue_min:float = 0 setget set_hue_min
export (float,0,1) var hue_max:float = 1 setget set_hue_max
export (float,0,1) var saturation_min:float = 0.5 setget set_saturation_min
export (float,0,1) var saturation_max:float = 0.7 setget set_saturation_max
export (float,0,1) var value_min:float = 1 setget set_value_min
export (float,0,1) var value_max:float = 1 setget set_value_max

func set_hue_min(f:float):
	hue_min = f
	if hue_max < hue_min:
		set_hue_max( hue_min )

func set_hue_max(f:float):
	hue_max = f
	if hue_min > hue_max:
		set_hue_min( hue_max )

func set_saturation_min(f:float):
	saturation_min = f
	if saturation_max < saturation_min:
		set_saturation_max( saturation_min )

func set_saturation_max(f:float):
	saturation_max = f
	if saturation_min > saturation_max:
		set_saturation_min( saturation_max )

func set_value_min(f:float):
	value_min = f
	if value_max < value_min:
		set_value_max( value_min )

func set_value_max(f:float):
	value_max = f
	if value_min > value_max:
		set_value_min( value_max )

func get_random():
	randomize()
	return Color.from_hsv( 
		rand_range(hue_min,hue_max),
		rand_range(saturation_min,saturation_max),
		rand_range(value_min,value_max)
	)

func shift(c:Color, h:float = 0, s:float = 0, v:float = 0 ):
	var ch:float = c.h + h
	while ch > 1:
		ch -= 1
	while ch < 0:
		ch += 1
	var cs:float = c.s + s
	while cs > 1:
		cs -= 1
	while cs < 0:
		cs += 1
	var cv:float = c.v + v
	while cv > 1:
		cv -= 1
	while cv < 0:
		cv += 1
	return Color.from_hsv( ch, cs, cv )

func get_inverse(c:Color, hmult:float = 1, smult:float = 1, vmult:float = 1 ):
	var h:float = c.h + 0.5
	if h > 1:
		h -=1
	return Color.from_hsv( h * hmult, c.s * smult, c.v * vmult )
	
