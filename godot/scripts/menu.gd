extends VBoxContainer

onready var seeds:Node = get_node( "/root/seeds" )

export (NodePath) var json_loader:NodePath
export (NodePath) var pager:NodePath
export (float,0,10) var drag_multiply:float = 3
export (float,0,10) var position_speed:float = 3

onready var jloader:Node = 		get_node(json_loader)
onready var pgr:Node = 			get_node(pager)
onready var vp_size:Vector2 = 	get_viewport().size

var resize:bool = true
var loaded:bool = false
var dragging:bool = false
var has_focus:bool = false
onready var position_target:Vector2 = rect_position
onready var position_current:Vector2 = rect_position

func purge():
	dragging = false
	while $titles.get_child_count() > 0:
		$titles.remove_child($titles.get_child(0))
	while $buttons.get_child_count() > 0:
		$buttons.remove_child($buttons.get_child(0))
	while $content.get_child_count() > 0:
		$content.remove_child($content.get_child(0))
	rect_min_size.x = 0
	
func generate():
	purge()
	if jloader == null:
		error( "json_loader is NOT defined!" )
		return
	var intro = jloader.get_page(0)
	if intro == null:
		#error( "Loading text" )
		loaded = false
		return
	add_title(intro.titre[0].content)
	add_subtitle(intro.titre[1].content)
	#for entry in intro.titre:
		#add_title( entry.content )
	for entry in intro.sommaire:
		add_button( entry.content )
	for entry in intro.contenu:
		if 'content' in entry:
			add_paragraph( entry.content )
	loaded = true

func add_title(s:String):
	var ttl:Label = seeds.MENU_LABEL.instance()
	ttl.text = str(s)
	ttl.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random( 'title' ) )
	var fc = seeds.COLOR_GENERATOR.get_random()
	var bc = Color(0, 0, 0)
	ttl.set( "custom_colors/font_color", fc )
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = bc
	norm_styl.content_margin_top = 20
	norm_styl.content_margin_right = 15
	norm_styl.content_margin_bottom = 15
	norm_styl.content_margin_left = 25
	ttl.set( "custom_styles/normal", norm_styl )
	ttl.mouse_filter = MOUSE_FILTER_STOP
	$titles.add_child( ttl )
# warning-ignore:return_value_discarded
	ttl.connect( "element_pressed", self, "element_pressed" )
# warning-ignore:return_value_discarded
	ttl.connect( "element_clicked", self, "element_clicked" )
# warning-ignore:return_value_discarded
	ttl.connect("mouse_entered",self,"element_in")
# warning-ignore:return_value_discarded
	ttl.connect("mouse_exited",self,"element_out")
	
func add_subtitle(s:String):
	var ttl:Label = seeds.MENU_LABEL.instance()
	ttl.text = s
	ttl.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random( 'subtitle' ) )
	#var fc = seeds.COLOR_GENERATOR.get_random()
	#var bc = seeds.COLOR_GENERATOR.get_inverse(fc,1,0.8,0.1)
	var fc = Color(255, 255, 255)
	var bc = Color(0, 0, 0)
	ttl.set( "custom_colors/font_color", fc )
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = bc
	norm_styl.content_margin_top = -15
	norm_styl.content_margin_right = 60
	norm_styl.content_margin_bottom = 25
	norm_styl.content_margin_left = 60
	ttl.set( "custom_styles/normal", norm_styl )
	$titles.add_child( ttl )
# warning-ignore:return_value_discarded
	ttl.connect( "element_pressed", self, "element_pressed" )
# warning-ignore:return_value_discarded
	ttl.connect( "element_clicked", self, "element_clicked" )
# warning-ignore:return_value_discarded
	ttl.connect("mouse_entered",self,"element_in")
# warning-ignore:return_value_discarded
	ttl.connect("mouse_exited",self,"element_out")

func add_paragraph(s:String):
	var pgph:Label = seeds.MENU_LABEL.instance()
	pgph.autowrap = true
	pgph.text = s
	pgph.set( "custom_fonts/font", seeds.FONT_GENERATOR.get_random() )
	var fc = seeds.COLOR_GENERATOR.get_random()
	var bc = seeds.COLOR_GENERATOR.get_inverse(fc,1,0.8,0.1)
	pgph.set( "custom_colors/font_color", fc )
	var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
	norm_styl.bg_color = bc
	norm_styl.content_margin_top = 25
	norm_styl.content_margin_right = 70
	norm_styl.content_margin_bottom = 25
	norm_styl.content_margin_left = 70
	pgph.set( "custom_styles/normal", norm_styl )
	$content.add_child( pgph )
# warning-ignore:return_value_discarded
	pgph.connect( "element_pressed", self, "element_pressed" )
# warning-ignore:return_value_discarded
	pgph.connect( "element_clicked", self, "element_clicked" )
# warning-ignore:return_value_discarded
	pgph.connect("mouse_entered",self,"element_in")
# warning-ignore:return_value_discarded
	pgph.connect("mouse_exited",self,"element_out")

func add_button(s:String):
	var btn:Control = seeds.MENU_BUTTON.instance()
	btn.text = s
# warning-ignore:return_value_discarded
	btn.connect( "element_pressed", self, "element_pressed" )
# warning-ignore:return_value_discarded
	btn.connect( "element_clicked", self, "element_clicked" )
# warning-ignore:return_value_discarded
	btn.connect( "mouse_in", self, "element_in" )
# warning-ignore:return_value_discarded
	btn.connect( "mouse_out", self, "element_out" )
	$buttons.add_child( btn )

func error(s:String):
	var err:Label = Label.new()
	err.text = s
	$content.add_child( err )

func element_pressed(b:Control):
	if !has_focus:
		has_focus = seeds.focus_lock( self )

func element_clicked(b:Control):
	var sc = b.get_script()
	if sc == null or sc.resource_path.find( "menu_button.gd" ) == -1:
		return
	print( sc.resource_path )
	var id:int = -1
	var i:int = 0
	for c in $buttons.get_children():
		if c == b:
			id = i
			break
		i += 1
	if id != -1:
		# releasing focus lock
		seeds.focus_unlock( self )
		pgr.load_page( id+1 )

# warning-ignore:unused_argument
func element_in(b:Control = null):
	pass

# warning-ignore:unused_argument
func element_out(b:Control = null):
	if dragging:
		return
	has_focus = false
	# losing focus only if not dragging
	if !dragging:
		seeds.focus_unlock( self )

# focus callbacks
func focus_drag_start():
	if !dragging:
		dragging = true
		for b in $buttons.get_children():
			b.activate( false )

func focus_drag_end():
	if dragging:
		dragging = false
		has_focus = false
		for b in $buttons.get_children():
			b.activate( true )

func focus_drag( amount:Vector2 ):
	position_target += amount * drag_multiply
	check_position_target()

func focus_lost():
	if has_focus:
		has_focus = false
		for b in $buttons.get_children():
			b.activate( false )

func focus_free():
	for b in $buttons.get_children():
		b.activate( true )

func reload():
	generate()

func vp_changed():
	vp_size = get_viewport().size
	check_position_target()
	resize = true

func check_position_target():
	if position_target.x > 0:
		position_target.x = 0
	elif rect_size.x >= vp_size.x and position_target.x + rect_size.x < vp_size.x:
		position_target.x = vp_size.x - rect_size.x
	if position_target.y > 0:
		position_target.y = 0
	elif rect_size.y >= vp_size.y and position_target.y + rect_size.y < vp_size.y:
		position_target.y = vp_size.y - rect_size.y

func _ready():
# warning-ignore:return_value_discarded
	get_viewport().connect("size_changed",self,"vp_changed")
	#if jloader != null:
# warning-ignore:return_value_discarded
		#jloader.connect( "data_changed", self, "reload" )
	#generate()
	# registering self as focus listener
	seeds.focus_register( self )

# warning-ignore:unused_argument
func _process(delta):
	
	if !loaded:
		return
	
	if resize:
		resize = false
		$titles.rect_min_size.x = vp_size.x
		$buttons.rect_min_size.x = vp_size.x
		$content.rect_min_size.x = vp_size.x
	
	if position_current != position_target:
		position_current += ( position_target - position_current ) * delta * position_speed
		var diff:Vector2 = ( position_target - position_current )
		if abs(diff.x) < 1:
			position_current.x = position_target.x
		if abs(diff.y) < 1:
			position_current.y = position_target.y
		rect_position = position_current

func _exit_tree():
	# unregistering self from focus listeners
	seeds.focus_unregister( self )
