tool

extends TextureRect

signal image_size_changed

onready var seeds = get_node("/root/seeds")

export (String) var url:String = ''
export (Vector2) var max_size:Vector2 = Vector2(500,500)
export (bool) var autoresize:bool = true
export (bool) var randomize_color:bool = true setget set_randomize_color
export (bool) var black_and_white:bool = true setget set_black_and_white

onready var http_request:HTTPRequest = $req

var lgen:Node = null
var cache_path:String = ''
var active_request:bool = false

func set_url(s:String):
	url = s
	cache_path = ''
	active_request = false
	if url.begins_with('http'):
		cache_path = seeds.IMG_CACHE_FOLDER + url.substr(url.find_last('//')+2).replace('/','.')
		# testing if file has already been downloaded
		var f:File = File.new()
		if f.file_exists(cache_path):
			load_image()
			f.close()
			return
		f.close()
		http_request.download_file = cache_path
		var error = http_request.request(url)
		if error != OK:
			print( http_request, " failed to contact " + url )
		else:
			active_request = true
			print( http_request, " downloading " + url )
		load_image()

func set_randomize_color(b:bool):
	randomize_color = b
	if material is ShaderMaterial:
		if material.shader.resource_path.ends_with("hsv_shift.shader"):
			if randomize_color:
				randomize()
				material.set_shader_param( "hue_shift", rand_range(0,1) )
			else:
				material.set_shader_param( "hue_shift", 0 )

func set_black_and_white(b:bool):
	black_and_white = b
	if material is ShaderMaterial:
		if material.shader.resource_path.ends_with("hsv_shift.shader"):
			if black_and_white:
				randomize()
				material.set_shader_param( "bw", 1 )
			else:
				material.set_shader_param( "bw", 0 )

func load_image():
	if cache_path == '':
		texture = null
		rect_size = Vector2(10,10)
	var f:File = File.new()
	if f.file_exists(cache_path):
		texture = ImageTexture.new()
		texture.load( cache_path )
		if autoresize:
			rect_size = texture.get_size()
		if rect_size.x > max_size.x:
			rect_size.y /= rect_size.x / max_size.x
			rect_size.x = max_size.x
		if rect_size.y > max_size.y:
			rect_size.x /= rect_size.y / max_size.y
			rect_size.y = max_size.y
		material = material.duplicate()
		set_randomize_color(randomize_color)
		set_black_and_white(black_and_white)
		emit_signal( "image_size_changed", self )
	elif lgen != null:
		texture = lgen.get_texture()
		lgen.request_push(self)
		rect_size = Vector2.ONE * lgen.size
		emit_signal( "image_size_changed", self )
	f.close()

func push_texture( it:ImageTexture ):
	if active_request:
		texture = it

# warning-ignore:unused_argument
func _http_request_completed(result,response_code,headers,body):
	active_request = false
	if cache_path == '' or response_code != 200:
		return
	load_image()

func _ready():
	pass

# call this once the node is stable in the tree!!!
func start():
	http_request.connect("request_completed",self,"_http_request_completed")
	set_url(url)

func _process(delta):
	if active_request:
		if http_request.get_http_client_status() == HTTPClient.STATUS_DISCONNECTED:
			http_request.cancel_request()
			active_request = false

func _exit_tree():
	http_request.cancel_request()
	if lgen != null:
		lgen.request_remove(self)
