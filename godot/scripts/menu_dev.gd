tool

extends VBoxContainer

export (NodePath) var font_generator:NodePath
export (NodePath) var color_generator:NodePath
export (NodePath) var json_loader:NodePath
export (bool) var generate:bool = false setget do_generate

onready var fgen:Node = get_node(font_generator)
onready var cgen:Node = get_node(color_generator)
onready var jloader:Node = get_node(json_loader)
onready var vp_size:Vector2 = get_viewport().size

func do_generate(b:bool):
	generate = false
	if b:
		while $titles.get_child_count() > 0:
			$titles.remove_child($titles.get_child(0))
		while $buttons.get_child_count() > 0:
			$buttons.remove_child($buttons.get_child(0))
		while $content.get_child_count() > 0:
			$content.remove_child($content.get_child(0))
		rect_size = Vector2(10,10)
		if fgen == null or cgen == null or jloader == null:
			fgen = get_node(font_generator)
			cgen = get_node(color_generator)
			jloader = get_node(json_loader)
		if fgen == null or cgen == null or jloader == null:
			error( "One of the nodepath is NOT defined!" )
			return
		var intro = jloader.get_page(0)
		if intro == null:
			error( "Waiting for data.." )
			return
		for entry in intro.titre:
			add_title( entry.content )
		for entry in intro.sommaire:
			add_button( entry.content )

func add_title(s:String):
	var ttl:Label = Label.new()
	ttl.text = s
	if fgen != null:
		ttl.set( "custom_fonts/font", fgen.get_random() )
	if cgen != null:
		var fc = cgen.get_random()
		var bc = cgen.get_inverse(fc,1,0.8,0.1)
		ttl.set( "custom_colors/font_color", fc )
		var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
		norm_styl.bg_color = bc
		norm_styl.content_margin_top = 10
		norm_styl.content_margin_right = 15
		norm_styl.content_margin_bottom = 10
		norm_styl.content_margin_left = 15
		ttl.set( "custom_styles/normal", norm_styl )
	$titles.add_child( ttl )
	ttl.owner = self.owner

func add_button(s:String):
	var btn:Button = Button.new()
	btn.text = s
	btn.align = Button.ALIGN_LEFT
	if fgen != null:
		btn.set( "custom_fonts/font", fgen.get_random() )
	if cgen != null:
		var fc = cgen.get_random()
		var bc = cgen.get_inverse(fc,1,0.9,0.8)
		btn.set( "custom_colors/font_color", fc )
		btn.set( "custom_colors/font_color_hover", cgen.get_inverse(fc,1,1,0.3) )
		var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
		norm_styl.bg_color = bc
		norm_styl.content_margin_top = 5
		norm_styl.content_margin_right = 5
		norm_styl.content_margin_bottom = 5
		norm_styl.content_margin_left = 5
		btn.set( "custom_styles/normal", norm_styl )
		var hover_styl:StyleBoxFlat = StyleBoxFlat.new()
		hover_styl.bg_color = fc
		hover_styl.content_margin_top = 5
		hover_styl.content_margin_right = 5
		hover_styl.content_margin_bottom = 5
		hover_styl.content_margin_left = 5
		btn.set( "custom_styles/hover", hover_styl )
	$buttons.add_child( btn )
	btn.owner = self.owner
# warning-ignore:return_value_discarded
	btn.connect("pressed",self,"reload")

func error(s:String):
	var err:Label = Label.new()
	err.text = s
	$content.add_child( err )

func reload():
	do_generate(true)

func _ready():
	if jloader != null:
# warning-ignore:return_value_discarded
		jloader.connect( "data_changed", self, "reload" )
	do_generate(true)
