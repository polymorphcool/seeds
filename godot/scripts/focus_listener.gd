extends Node

var mouse_in:bool = false
var has_focus:bool = false

# called when seeds detect the start of drag
func focus_drag_start():
	has_focus = true

# called when seeds detect the end of drag
func focus_drag_end():
	has_focus = false

# called when current node is dragged
func focus_drag(amount:Vector2):
	pass

# called when a node is granted to lock the focus
func focus_lost():
	has_focus = false
