tool

extends GridContainer

export (NodePath) var font_generator:NodePath = ''
export (NodePath) var color_generator:NodePath = ''
export (int,0,100) var number:int = 10 setget set_number
export (bool) var generate:bool = false setget do_generate

func set_number(i:int):
	number = i
	do_generate(true)

func do_generate(b:bool):
	generate = false
	if b:
		var fg = get_node( font_generator )
		var cg = get_node( color_generator )
		while get_child_count() > 0:
			remove_child(get_child(0))
		for i in range(0,number):
			var btn:Button = Button.new()
			for l in range( 0,rand_range(5,10) ):
				btn.text += char( 65 + 32 + rand_range(0,26) )
			add_child( btn )
			btn.owner = self.owner
			if fg != null:
				btn.set( "custom_fonts/font", fg.get_random() )
			if cg != null:
				var fc = cg.get_random()
				var bc = cg.get_inverse(fc,1,0.8,0.8)
				btn.set( "custom_colors/font_color", fc )
				btn.set( "custom_colors/font_color_hover", cg.get_inverse(fc,1,1,0.3) )
				var norm_styl:StyleBoxFlat = StyleBoxFlat.new()
				norm_styl.bg_color = bc
				norm_styl.content_margin_top = 5
				norm_styl.content_margin_right = 5
				norm_styl.content_margin_bottom = 5
				norm_styl.content_margin_left = 5
				btn.set( "custom_styles/normal", norm_styl )
				var hover_styl:StyleBoxFlat = StyleBoxFlat.new()
				hover_styl.bg_color = fc
				hover_styl.content_margin_top = 5
				hover_styl.content_margin_right = 5
				hover_styl.content_margin_bottom = 5
				hover_styl.content_margin_left = 5
				btn.set( "custom_styles/hover", hover_styl )

func _ready():
	do_generate(true)
