shader_type canvas_item;
render_mode blend_mix;

uniform vec4 dynamic_color0:hint_color;
uniform vec4 dynamic_color1:hint_color;
uniform float mixer = 1.0;

uniform float multiplier_x = 20.;
uniform float multiplier_y = 200.;
uniform float repeat = 5.;
uniform float time = 0;

void fragment() {
	float d = UV.x;
	if ( UV.x > 0.5 ) {
		d = 0.5-(d-0.5);
	}
	d *= 2.;
	float m = (time+d*multiplier_x+UV.y*multiplier_y)*repeat;
	COLOR = mix( dynamic_color0, dynamic_color1, (1.+sin(m))*.5 * mixer );
}