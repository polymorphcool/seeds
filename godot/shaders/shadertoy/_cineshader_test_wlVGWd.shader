//afl_ext 2017-2019
//https://www.shadertoy.com/view/MdXyzX

shader_type canvas_item;
render_mode blend_mix;

const mat2 m2 = mat2(vec2(0.8,-0.6),vec2(0.6,0.8));

uniform vec4 static_color:hint_color;
uniform vec4 dynamic_color0:hint_color;
uniform vec4 dynamic_color1:hint_color;
uniform float dynamic:hint_range(0,1);
uniform vec2 uv_offset;
uniform vec2 uv_mult;
uniform vec2 uv_center = vec2(.5);
uniform float speed;
uniform float screen_ratio = 1.0;
uniform float color_multiply = 1.4;
uniform float vignette = 1.0;
uniform float lentille_power:hint_range(1,100);
uniform float lentille_strength:hint_range(0,100);

float rand(vec2 n) {
    return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

float noise(vec2 p) {
    vec2 ip = floor(p);
    vec2 u = fract(p);
    u = u*u*(3.0-2.0*u);
    float res = mix(
        mix(rand(ip),rand(ip+vec2(1.0,0.0)),u.x),
        mix(rand(ip+vec2(0.0,1.0)),rand(ip+vec2(1.0,1.0)),u.x),u.y);
    return res*res;
}

float fbm( in vec2 p ){
    float f = 0.0;
    f += 0.5000*noise( p ); p = m2*p*2.02;
    f += 0.2500*noise( p ); p = m2*p*2.03;
    f += 0.1250*noise( p ); p = m2*p*2.01;
    f += 0.0625*noise( p );
    return f/0.769;
}

float pattern( in vec2 p, float time ) {
  vec2 q = vec2(fbm(p + vec2(0.0,0.0)));
  vec2 r = vec2(fbm( p + 4.0*q + vec2(1.7,9.2)));
  r+= time * 0.15;
  return fbm( p + 1.760*r );
}

void fragment() {
	vec2 px2center = (UV - uv_center) * vec2( 1., screen_ratio );
	float dist2center = min(0.5,length(px2center)) * 2.;
	px2center = normalize(px2center) * lentille_strength;
  	vec2 uv = (UV - px2center * pow((1.-dist2center),lentille_power)) * uv_mult + uv_offset;
  	float displacement = pattern( uv, TIME * speed ); // * sin(1.0-dist2center) );
	vec4 cmix = mix( dynamic_color0, dynamic_color1, displacement * color_multiply ) * vec4(vec3(1.0-dist2center*vignette),1.0);
	COLOR = mix( static_color, cmix, dynamic );
}