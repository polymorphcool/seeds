shader_type canvas_item;
render_mode blend_mix;

uniform vec4 dynamic_color0:hint_color;
uniform vec4 dynamic_color1:hint_color;
uniform float frequency = 20.0;
uniform float speed = 3.0;
uniform float radius = 3.0;

void fragment() {
	float ct = cos(TIME);
	float st = sin(TIME);
	vec2 c = UV-vec2(.5+ct*radius,.5+st*radius);
	float d = length(c);
	COLOR = mix( 
		dynamic_color0,//*(.5+ct)*.5, 
		dynamic_color1, //+vec4(vec3(st*.2),1.),
		(1.0+sin(TIME*speed-d*pow(d*1.1,2.)*frequency))*.5/d*3. );
}