shader_type canvas_item;
render_mode blend_mix;

uniform vec4 static_color:hint_color;
uniform vec4 dynamic_color0:hint_color;
uniform vec4 dynamic_color1:hint_color;
uniform float dynamic:hint_range(0,1);

uniform vec2 uv_offset;
uniform vec2 uv_mult;

void fragment() {
	if (dynamic > 0.0) {
		float s = (1.0 + sin((UV.x-uv_offset.x*TIME)*uv_mult.x)) * 0.5;
		s *= (1.0 + sin((UV.y-uv_offset.y*TIME)*uv_mult.y)) * 0.5;
		float cshift = (1.0+sin(UV.x-TIME*2.0)) * 0.5;
		vec4 cmix = mix(mix(dynamic_color0,dynamic_color1,cshift),mix(dynamic_color0,dynamic_color1,1.0-cshift),s);
		COLOR = mix( static_color, cmix, dynamic );
	} else {
		COLOR = static_color;
	}
}