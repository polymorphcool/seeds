shader_type canvas_item;
render_mode blend_mix;

uniform float hue_shift:hint_range(0,1);
uniform float saturation_shift:hint_range(0,1);
uniform float value_shift:hint_range(0,1);
uniform float bw:hint_range(0,1);
uniform float hue_shift_speed = 0.;
uniform float saturation_shift_speed = 0.;
uniform float value_shift_speed = 0.;

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void fragment() {
	vec4 color = texture( TEXTURE, UV );
	vec3 hsv = rgb2hsv(color.xyz);
	hsv.x += hue_shift + TIME*hue_shift_speed; 					while (hsv.x>1.0) { hsv.x -= 1.0; }
	hsv.y += saturation_shift + TIME*saturation_shift_speed; 	while (hsv.y>1.0) { hsv.y -= 1.0; }
	hsv.z += value_shift + TIME*value_shift_speed; 				while (hsv.z>1.0) { hsv.z -= 1.0; }
	vec3 rgb = hsv2rgb(hsv);
	float lum = 0.21 * rgb.r + 0.71 * rgb.g + 0.07 * rgb.b;
	COLOR = vec4( mix(rgb,vec3(lum),bw), color.a );
}