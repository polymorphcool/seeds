shader_type canvas_item;
render_mode blend_mix;

uniform vec4 static_color:hint_color;
uniform float ratio = 1.0;
uniform float radius = 0.0;
uniform float radius_offset = 1.0;

void fragment() {
	float d = pow((1.-min(1.,length((UV-vec2(.5))*vec2(1.,ratio))))*3.,4.);
	COLOR = 
		mix(
			static_color, 
			texture(TEXTURE,UV), 
			min(1.,max(0.,d * radius-radius_offset))
		) * vec4(vec3(1.), min(1.,d * radius));
}