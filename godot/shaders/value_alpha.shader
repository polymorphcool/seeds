shader_type canvas_item;
render_mode blend_mix;

uniform vec4 static_color:hint_color;
uniform float limit = 1.0;

vec3 rgb2hsv(vec3 c) {
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

void fragment() {
	vec4 c = texture(TEXTURE,UV);
	vec3 hsv = rgb2hsv(c.xyz);
	float d = limit - hsv.z;
	d = max(0.,min(1.,d * 1000.));
	COLOR = mix(static_color,vec4( c.xyz, d ),d);
}