# SEEDS

an attempt to create a generative layout for [seeds](http://www.procjam.com/seeds/)

submission at procjam 2020.

executable is available on [itch.io](https://polymorphcool.itch.io/seeds)
